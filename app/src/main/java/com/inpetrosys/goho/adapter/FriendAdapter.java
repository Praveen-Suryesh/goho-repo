package com.inpetrosys.goho.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.inpetrosys.goho.Interfaceuser.OnClickerListener;
import com.inpetrosys.goho.R;
import com.inpetrosys.goho.beans.Friend;

import java.util.ArrayList;

public class FriendAdapter extends RecyclerView.Adapter<FriendAdapter.ViewHolder> {
    private Context context;
    private ArrayList<Friend> friendArrayList;
    private OnClickerListener listener;

    public FriendAdapter(Context context, ArrayList<Friend> friendArrayList, OnClickerListener listener) {
        this.context = context;
        this.friendArrayList = friendArrayList;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.friend_row, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return friendArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView img_friendimg;
        private ImageView img_optionfriend;

        public ViewHolder(View itemView) {
            super(itemView);
            img_friendimg=itemView.findViewById(R.id.img_friendimg);
            img_optionfriend=itemView.findViewById(R.id.img_optionfriend);
        }
    }
}
