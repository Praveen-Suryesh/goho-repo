package com.inpetrosys.goho.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.inpetrosys.goho.Interfaceuser.OnClickerListener;
import com.inpetrosys.goho.R;
import com.inpetrosys.goho.beans.Friend;

import java.util.ArrayList;

public class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.ViewHolder> {
    private Context context;
    private ArrayList<Friend> contactArrayList;
    private OnClickerListener listener;

    public ContactAdapter(Context context, ArrayList<Friend> contactArrayList, OnClickerListener listener) {
        this.context = context;
        this.contactArrayList = contactArrayList;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.contact_row, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Friend friend = contactArrayList.get(position);
        if (friend.isChk())
            holder.chk_frind.setChecked(true);
        else
            holder.chk_frind.setChecked(false);
    }

    @Override
    public int getItemCount() {
        return contactArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView img_friend;
        private TextView tv_friendName;
        private CheckBox chk_frind;

        public ViewHolder(View itemView) {
            super(itemView);
            img_friend = itemView.findViewById(R.id.img_friend);
            tv_friendName = itemView.findViewById(R.id.tv_friendName);
            chk_frind = itemView.findViewById(R.id.chk_frind);
        }
    }
}
