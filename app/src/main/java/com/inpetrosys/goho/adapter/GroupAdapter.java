package com.inpetrosys.goho.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.inpetrosys.goho.Interfaceuser.OnClickerListener;
import com.inpetrosys.goho.R;
import com.inpetrosys.goho.beans.Friend;
import com.inpetrosys.goho.beans.Group;

import java.util.ArrayList;

public class GroupAdapter extends RecyclerView.Adapter<GroupAdapter.ViewHolder> {
    private Context context;
    private ArrayList<Group> groupArrayList;
    private OnClickerListener listener;
    private boolean onTime;

    public GroupAdapter(Context context, ArrayList<Group> groupArrayList, OnClickerListener listener) {
        this.context = context;
        this.groupArrayList = groupArrayList;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.group_row, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        int height = parent.getMeasuredHeight();
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ArrayList<Friend> friendArrayList = groupArrayList.get(position).getFriendArrayList();
       if(friendArrayList.size()>0) {
           holder.imgLayout1.removeAllViews();
           for (int j = 0; j < friendArrayList.size(); j++) {
               create_img1(holder.imgLayout1);
           }
       }else{
           holder.imgLayout1.removeAllViews();
       }

       if(onTime){

       }
    }

    void create_img1(LinearLayout imgLayout1) {
        LinearLayout.LayoutParams parms = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        parms.gravity = Gravity.CENTER;
        final ImageView imageView = new ImageView(context);
        parms.setMargins(5, 0, 5, 0);
        imageView.setLayoutParams(parms);

        imageView.setImageResource(R.drawable.defult_user);
        imgLayout1.addView(imageView);
    }

    @Override
    public int getItemCount() {
        return groupArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_groupname;
        private HorizontalScrollView hv_friendList;
        private LinearLayout imgLayout1;

        public ViewHolder(View itemView) {
            super(itemView);
            tv_groupname = itemView.findViewById(R.id.tv_groupname);
            hv_friendList = itemView.findViewById(R.id.hv_friendList);
            imgLayout1 = itemView.findViewById(R.id.imgLayout1);
        }
    }
}
