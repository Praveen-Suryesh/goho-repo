package com.inpetrosys.goho.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.inpetrosys.goho.Interfaceuser.OnClickerListener;
import com.inpetrosys.goho.R;
import com.inpetrosys.goho.beans.Friend;
import com.inpetrosys.goho.beans.Group;
import com.inpetrosys.goho.beans.Post;

import java.util.ArrayList;

public class PostAdapter extends RecyclerView.Adapter<PostAdapter.ViewHolder> {
    private Context context;
    private ArrayList<Post> postArrayList;
    private OnClickerListener listener;
    private boolean onTime;

    public PostAdapter(Context context, ArrayList<Post> postArrayList, OnClickerListener listener) {
        this.context = context;
        this.postArrayList = postArrayList;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.post_row, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
    }


    @Override
    public int getItemCount() {
        return postArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView img_post;
        private TextView post_name, txt_edit, txt_location_post1, txt_location_post2, post_desc;

        public ViewHolder(View itemView) {
            super(itemView);
            img_post = itemView.findViewById(R.id.img_post);
            post_name = itemView.findViewById(R.id.post_name);
            txt_edit = itemView.findViewById(R.id.txt_edit);
            txt_location_post1 = itemView.findViewById(R.id.txt_location_post1);
            txt_location_post2 = itemView.findViewById(R.id.txt_location_post2);
            post_desc = itemView.findViewById(R.id.post_desc);
        }
    }
}
