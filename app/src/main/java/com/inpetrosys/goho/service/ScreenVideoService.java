//package com.inpetrosys.goho.service;
//
//import android.app.IntentService;
//import android.content.Intent;
//import android.graphics.Bitmap;
//import android.graphics.Canvas;
//import android.graphics.Matrix;
//import android.os.Environment;
//import android.os.Handler;
//import android.support.annotation.Nullable;
//import android.view.View;
//
//import com.google.android.gms.maps.GoogleMap;
//import com.inpetrosys.goho.Interfaceuser.BlurScreenCallback;
//
//import java.io.File;
//import java.io.FileOutputStream;
//import java.util.ArrayList;
//
//public class ScreenVideoService extends IntentService {
//    private ArrayList<Bitmap> bitmapArrayList=new ArrayList<>();
//    /**
//     * Creates an IntentService.  Invoked by your subclass's constructor.
//     *
//     * @param name Used to name the worker thread, important only for debugging.
//     */
//    public ScreenVideoService() {
//        super("ScreenVideoService");
//    }
//    @Override
//    protected void onHandleIntent(@Nullable Intent intent) {
//        final Handler ha=new Handler();
//        ha.postDelayed(new Runnable() {
//
//            @Override
//            public void run() {
//                //call function
//                takeScreenShot();
//                ha.postDelayed(this, 10000);
//            }
//        }, 10000);
//    }
//
//    public void takeScreenShot(final View view, GoogleMap mMap, final BlurScreenCallback screenCallback) {
//
//        GoogleMap.SnapshotReadyCallback callback = new GoogleMap.SnapshotReadyCallback() {
//
//
//            @Override
//            public void onSnapshotReady(Bitmap snapshot) {
//
//                try {
//                    view.setDrawingCacheEnabled(true);
//                    Bitmap backBitmap = view.getDrawingCache();
//                    Bitmap bmOverlay = Bitmap.createBitmap(
//                            backBitmap.getWidth(), backBitmap.getHeight(),
//                             backBitmap.getConfig());
//                    Canvas canvas = new Canvas(bmOverlay);
//                    canvas.drawBitmap(snapshot, new Matrix(), null);
//                    canvas.drawBitmap(backBitmap, 0, 0, null);
//                    // image naming and path  to include sd card  appending name you choose for file
////                    String mPath = Environment.getExternalStorageDirectory().toString() + "Goho/Screenshots/" + System.currentTimeMillis() + ".png";
//                    if (isSaveFile) {
//                        File fileDir = new File(Environment.getExternalStorageDirectory(), "GOHO");
//                        if (!fileDir.exists())
//                            fileDir.mkdirs();
//
//                        String path = "/GOHO_Screen_"
//                                + System.currentTimeMillis() + ".png";
//                        File file = new File(fileDir, path);
//
//                        FileOutputStream out = new FileOutputStream(file);
//                        bmOverlay.compress(Bitmap.CompressFormat.PNG, 90, out);
//                        out.flush();
//                        out.close();
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        };
//
//        mMap.snapshot(callback);
//    }
//}
