package com.inpetrosys.goho.activity;

import android.content.Context;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolylineOptions;
import com.inpetrosys.goho.Interfaceuser.HomePresenterinterface;
import com.inpetrosys.goho.Interfaceuser.WeathertempInterface;
import com.inpetrosys.goho.apicalling.APIClient;
import com.inpetrosys.goho.apicalling.APIInterface;
import com.inpetrosys.goho.beans.Main;
import com.inpetrosys.goho.beans.MainDetails;
import com.inpetrosys.goho.utility.DirectionsJSONParser;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomePresenter {
    private static APIInterface apiInterface = null;
    private HomePresenterinterface homePresenterinterface;
    private Context context;

    HomePresenter(Context context, HomePresenterinterface homePresenterinterface) {
        this.homePresenterinterface = homePresenterinterface;
        this.context = context;
        apiInterface = APIClient.getClient().create(APIInterface.class);
    }

    public void callPolyLine(LatLng origin, LatLng dest) {

        // Getting URL to the Google Directions API
        String url = getDirectionsUrl(origin, dest);

        DownloadTask downloadTask = new DownloadTask();

        // Start downloading json data from Google Directions API
        downloadTask.execute(url);
    }

    static String[] getAddress(Context context, double lat, double lng) throws IOException {
        String[] loc = new String[2];
        Geocoder gcd = new Geocoder(context, Locale.getDefault());
        List<Address> addresses = gcd.getFromLocation(lat, lng, 1);
        if (addresses.size() > 0) {
            System.out.println(addresses.get(0).getLocality());
            loc[0] = addresses.get(0).getLocality();
            loc[1] = addresses.get(0).getCountryCode();
            return loc;
        } else {
            // do your stuff
            return loc;
        }
    }

    public static class WeatherAsyn extends AsyncTask<Void, Void, Void> {
        private WeathertempInterface weathertempInterface;
        private double latitude;
        private double longitude;
        private Context context;

        public WeatherAsyn(Context context, WeathertempInterface weathertempInterface, double latitude, double longitude) {
            this.weathertempInterface = weathertempInterface;
            this.latitude = latitude;
            this.longitude = longitude;
            this.context = context;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                getWeatherTemp(context, weathertempInterface, latitude, longitude);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }
    }

    public static void getWeatherTemp(Context context, final WeathertempInterface weathertempInterface, double latitude, double longitude) throws IOException {

        String[] loc = getAddress(context, latitude, longitude);
        /**
         getTempature
         **/
        Call<Main> call = apiInterface.doGetListResources(loc[0] + "," + loc[1], "894f16277153c1a0e68d72e785e7f1fd");
        call.enqueue(new Callback<Main>() {
            @Override
            public void onResponse(Call<Main> call, Response<Main> response) {
                try {

                    Log.d("TAG", response.code() + "");
                    Main main = response.body();
                    MainDetails mainDetails = main.mainDetails;

                    if (String.valueOf(mainDetails.temp) != null && !String.valueOf(mainDetails.temp).equalsIgnoreCase("")) {
                        weathertempInterface.onBackTemp((mainDetails.temp-273.15));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<Main> call, Throwable t) {
                call.cancel();
            }
        });
    }

    private class DownloadTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... url) {

            String data = "";

            try {
                data = downloadUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserTask parserTask = new ParserTask();
            parserTask.execute(result);
        }
    }

    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();

                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList points = null;
            PolylineOptions lineOptions = null;

            for (int i = 0; i < result.size(); i++) {
                points = new ArrayList();
                lineOptions = new PolylineOptions();

                List<HashMap<String, String>> path = result.get(i);

                for (int j = 0; j < path.size(); j++) {
                    HashMap point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat").toString());
                    double lng = Double.parseDouble(point.get("lng").toString());
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                lineOptions.addAll(points);
                lineOptions.width(8);
                lineOptions.color(Color.BLACK);
                lineOptions.geodesic(true);

            }

// Drawing polyline in the Google Map for the i-th route
            homePresenterinterface.callBackPloyline(lineOptions);

        }
    }


    private String getDirectionsUrl(LatLng origin, LatLng dest) {

        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;

        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;

        // Sensor enabled
        String sensor = "sensor=false";
        String mode = "mode=driving";

        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor + "&" + mode;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters+"&Key=AIzaSyB4jgXi_JUUmA1q5LE43uLrcfceLQ6Klik";


        return url;
    }

    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            urlConnection = (HttpURLConnection) url.openConnection();

            urlConnection.connect();

            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        } catch (Exception e) {
            Log.d("Exception", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }
}
