package com.inpetrosys.goho.activity;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.hardware.display.DisplayManager;
import android.hardware.display.VirtualDisplay;
import android.location.Location;
import android.media.MediaRecorder;
import android.media.projection.MediaProjection;
import android.media.projection.MediaProjectionManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.os.ResultReceiver;
import android.os.UserHandle;
import android.provider.AlarmClock;
import android.provider.Settings;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.speech.tts.TextToSpeech;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.SparseIntArray;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.MultiTransformation;
import com.bumptech.glide.load.resource.bitmap.FitCenter;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.github.zagum.speechrecognitionview.RecognitionProgressView;
import com.github.zagum.speechrecognitionview.adapters.RecognitionListenerAdapter;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.inpetrosys.goho.Interfaceuser.BlurScreenCallback;
import com.inpetrosys.goho.Interfaceuser.CallBackInterface;
import com.inpetrosys.goho.Interfaceuser.HomePresenterinterface;
import com.inpetrosys.goho.Interfaceuser.OnClickerListener;
import com.inpetrosys.goho.Interfaceuser.OnGPSChooseListener;
import com.inpetrosys.goho.Interfaceuser.WeathertempInterface;
import com.inpetrosys.goho.R;
import com.inpetrosys.goho.adapter.FriendAdapter;
import com.inpetrosys.goho.beans.Friend;
import com.inpetrosys.goho.beans.Group;
import com.inpetrosys.goho.fragment.ContactListFragment;
import com.inpetrosys.goho.fragment.group.GroupGohoFragment;
import com.inpetrosys.goho.fragment.post.PostDialogFragment;
import com.inpetrosys.goho.fragment.setting.SettingDialogFragment;
import com.inpetrosys.goho.session.SharePrefencesUtil;
import com.inpetrosys.goho.utility.DirectionsJSONParser;
import com.inpetrosys.goho.service.GPSLocationService;
import com.inpetrosys.goho.receiver.MyResultReceiver;
import com.inpetrosys.goho.utility.UtilClass;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import jp.wasabeef.blurry.Blurry;
//import jp.wasabeef.glide.transformations.BlurTransformation;
//import zh.wang.android.yweathergetter4a.WeatherInfo;
//import zh.wang.android.yweathergetter4a.YahooWeather;
//import zh.wang.android.yweathergetter4a.YahooWeatherInfoListener;


public class HomeActivity extends FragmentActivity implements OnMapReadyCallback,
//        LocationListener,
        View.OnClickListener
//        , YahooWeatherInfoListener
        , HomePresenterinterface
        , CallBackInterface
        , WeathertempInterface
        , MyResultReceiver.Receiver
        , TextToSpeech.OnInitListener {

    private GoogleMap mMap;
    private ArrayList<LatLng> markerPoints = new ArrayList();
    private LinearLayout ll_uper;
    private TextView txt_time, txt_date, txt_temp, tv_timer_watch;
    //    private LocationManager locationManager;
//    private static final long MIN_TIME = 400;
//    private static final float MIN_DISTANCE = 1000;
    private SupportMapFragment mapFragment;
    private ImageView img_screen, tv_routeTime, img_screenshots, img_post, img_menu, img_route, img_drive_mode, img_contact_list, img_user, img_box, img_1, img_2, img_3;
    private boolean isOpen, isBoxOpen;
    private RelativeLayout mainContainer, boxcontainer;
    private ImageView img_add_friend, img_video, img_voice_record;
    private RecyclerView rv_friendList;
    private LinearLayoutManager layoutManager;
    private ArrayList<Friend> friendArrayList = new ArrayList<>();
    private FriendAdapter friendAdapter;
    private boolean iswatchClick;
    private HomePresenter homePresenter;
    private SharePrefencesUtil sharePrefencesUtil;
    private Bitmap bmp;
    private MarkerOptions options, options2, options3;
    private LatLng latLng;
    private View mCustomMarkerView;
    private ImageView profile_image;
    private ConstraintLayout constaint_main_view;

    private ImageButton img_play, img_pause, img_stop;
    private LinearLayout ll_videoView;
    private MyResultReceiver mReceiver;
    private Intent gpsIntent;
    private CountDownTimer countDownTimer;
    private Polyline ployLine;
    private UtilClass utilClass;
    private Handler ha;
    private Runnable runnable;


    //Record Screen

    private static final String TAG = "MainActivity";
    private static final int REQUEST_CODE = 1000;
    private int mScreenDensity;
    private MediaProjectionManager mProjectionManager;
    private static final int DISPLAY_WIDTH = 720;
    private static final int DISPLAY_HEIGHT = 1280;
    private MediaProjection mMediaProjection;
    private VirtualDisplay mVirtualDisplay;
    private MediaProjectionCallback mMediaProjectionCallback;
    private MediaRecorder mMediaRecorder;
    private static final SparseIntArray ORIENTATIONS = new SparseIntArray();
    private static final int REQUEST_PERMISSIONS = 10;

    //Alarm
//    private static final Uri ALARM_URI = Uri.parse("android-app://com.myclockapp/set_alarm_page");
//Voice Speech
    private final int REQ_CODE_SPEECH_INPUT = 100;
    private TextToSpeech tts;

    static {
        ORIENTATIONS.append(Surface.ROTATION_0, 90);
        ORIENTATIONS.append(Surface.ROTATION_90, 0);
        ORIENTATIONS.append(Surface.ROTATION_180, 270);
        ORIENTATIONS.append(Surface.ROTATION_270, 180);
    }

    private boolean isScreenCaptureRun = false;
    private EditText txt_speech;
    private ImageButton imgbtn_speakknow;
    private SpeechRecognizer speechRecognizer;
    private RecognitionProgressView recognitionProgressView;

    //    @Override
//    public void gotWeatherInfo(WeatherInfo weatherInfo, YahooWeather.ErrorType errorType) {
//        if (weatherInfo != null) {
//            txt_temp.setText(weatherInfo.getCurrentTemp() + "\u00b0" + "C");
//        }
//    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.home_activity);
        utilClass = UtilClass.getInstance();
        mReceiver = new MyResultReceiver(new Handler());
        mReceiver.setReceiver(this);


        speechRecognizer = SpeechRecognizer.createSpeechRecognizer(this);

        /**
         Create new user
         **/
//       User1 user = new User1("morpheus", "leader");
//        Call<User1> call1 = apiInterface.createUser(user);
//        call1.enqueue(new Callback<User1>() {
//            @Override
//            public void onResponse(Call<User1> call, Response<User1> response) {
//                User1 user1 = response.body();
//
//                Toast.makeText(getApplicationContext(), user1.name + " " + user1.job + " " + user1.id + " " + user1.createdAt, Toast.LENGTH_SHORT).show();
//
//            }
//
//            @Override
//            public void onFailure(Call<User1> call, Throwable t) {
//                call.cancel();
//            }
//        });

        /**
         GET List Users
         **/
//        Call<UserList> call2 = apiInterface.doGetUserList("2");
//        call2.enqueue(new Callback<UserList>() {
//            @Override
//            public void onResponse(Call<UserList> call, Response<UserList> response) {
//
//                UserList userList = response.body();
//                Integer text = userList.page;
//                Integer total = userList.total;
//                Integer totalPages = userList.totalPages;
//                List<UserList.Datum> datumList = userList.data;
//                Toast.makeText(getApplicationContext(), text + " page\n" + total + " total\n" + totalPages + " totalPages\n", Toast.LENGTH_SHORT).show();
//
//                for (UserList.Datum datum : datumList) {
//                    Toast.makeText(getApplicationContext(), "id : " + datum.id + " name: " + datum.first_name + " " + datum.last_name + " avatar: " + datum.avatar, Toast.LENGTH_SHORT).show();
//                }
//
//
//            }
//
//            @Override
//            public void onFailure(Call<UserList> call, Throwable t) {
//                call.cancel();
//            }
//        });


        /**
         POST name and job Url encoded.
         **/
//        Call<UserList> call3 = apiInterface.doCreateUserWithField("morpheus","leader");
//        call3.enqueue(new Callback<UserList>() {
//            @Override
//            public void onResponse(Call<UserList> call, Response<UserList> response) {
//                UserList userList = response.body();
//                Integer text = userList.page;
//                Integer total = userList.total;
//                Integer totalPages = userList.totalPages;
//                List<UserList.Datum> datumList = userList.data;
//                Toast.makeText(getApplicationContext(), text + " page\n" + total + " total\n" + totalPages + " totalPages\n", Toast.LENGTH_SHORT).show();
//
//                for (UserList.Datum datum : datumList) {
//                    Toast.makeText(getApplicationContext(), "id : " + datum.id + " name: " + datum.first_name + " " + datum.last_name + " avatar: " + datum.avatar, Toast.LENGTH_SHORT).show();
//                }
//
//            }
//
//            @Override
//            public void onFailure(Call<UserList> call, Throwable t) {
//                call.cancel();
//            }
//        });


        sharePrefencesUtil = new SharePrefencesUtil(this);
        homePresenter = new HomePresenter(HomeActivity.this, this);
//        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

//        try {
//            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
        // TODO: Consider calling
        //    ActivityCompat#requestPermissions
        // here to request the missing permissions, and then overriding
        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
        //                                          int[] grantResults)
        // to handle the case where the user grants the permission. See the documentation
        // for ActivityCompat#requestPermissions for more details.
//                return;
//            }
//            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MIN_TIME, MIN_DISTANCE, this); //You can also use LocationManager.GPS_PROVIDER and LocationManager.PASSIVE_PROVIDER
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
        mapFragment.getMapAsync(this);
        try {
            initView();
        } catch (Exception e) {
            e.printStackTrace();
        }
        loadData();

        // Get the intent
//        Intent intent = getIntent();
//        if (AlarmClock.ACTION_SET_ALARM.equals(intent.getAction())) {
//            if (intent.hasExtra(AlarmClock.EXTRA_HOUR)) {
//                // Step 2: get the rest of the intent extras and set an alarm
//            }
//
//            // Step 3: report the action through the App Indexing API
//            Thing alarm = new Thing.Builder()
//                    .setName("Alarm for 4:00 PM")
//                    .setDescription("Alarm set for 4:00 PM, with the 'Argon' ringtone"
//                            + " and vibrate turned on.")
//                    .setUrl(APP_URI)
//                    .build();
//
//            Action setAlarmAction = new Action.Builder(Action.TYPE_ADD)
//                    .setObject(alarm)
//                    .setActionStatus(Action.STATUS_TYPE_COMPLETED)
//                    .build();
//
//            AppIndex.AppIndexApi.end(mClient, setAlarmAction);
//        }


//        new OpenWeatherMapTask(
//                "Indore,India",
//                txt_temp).execute();

//        YahooWeather mYahooWeather = YahooWeather.getInstance();
//        mYahooWeather.queryYahooWeatherByPlaceName(this, "Indore India", this);
//Getting current latitude and longitude using network provider
//        Location location = getLocation();
//        if (location != null) {
//            double latitude = location.getLatitude();
//            double longitude = location.getLongitude();
//        }


    }

    @Override
    protected void onResume() {
        super.onResume();
        img_screenshots.setImageBitmap(null);
        if (gpsIntent == null) {
            gpsIntent = new Intent(this, GPSLocationService.class);
            gpsIntent.putExtra("receiverTag", mReceiver);
            startService(gpsIntent);
        }
    }

    @Override
    public void callBackPloyline(PolylineOptions polylineOptions) {
        try {
            ployLine = mMap.addPolyline(polylineOptions);

//            Toast.makeText(HomeActivity.this, "time==>" + DirectionsJSONParser.timedis + " and  dis==>" + DirectionsJSONParser.distance, Toast.LENGTH_LONG).show();
//            tv_timer_watch.setText(DirectionsJSONParser.timedis + "");
            DirectionsJSONParser.timedis = null;
            DirectionsJSONParser.distance = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onback() {
        // Create custom dialog object
//        Blurry.delete(constaint_main_view);
//        final View dialogView = View.inflate(this,R.layout.sharelocation_option_dialog,null);

        final Dialog dialog = new Dialog(this, R.style.dialog_slide_animation);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // Include dialog.xml file
        dialog.setContentView(R.layout.sharelocation_option_dialog);
        Button btn_yes = dialog.findViewById(R.id.btn_yes);
        btn_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                utilClass.perfomroneClick();
                dialog.dismiss();
//                Blurry.delete(constaint_main_view);
                UtilClass.hideSoftKeyboard(HomeActivity.this);
                try {
                    showBottomShareDialog();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        Button btn_no = dialog.findViewById(R.id.btn_no);
        btn_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                utilClass.perfomroneClick();
                dialog.dismiss();
//                revealShow(dialogView, false, dialog);
//                Blurry.delete(constaint_main_view);
                img_screenshots.setImageBitmap(null);
            }
        });
        int width = getResources().getDisplayMetrics().widthPixels;
        dialog.getWindow().setLayout(width - 100, ConstraintLayout.LayoutParams.WRAP_CONTENT);

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {
//                revealShow(dialogView, true, null);
            }
        });

        dialog.setOnKeyListener(new Dialog.OnKeyListener() {

            @Override
            public boolean onKey(DialogInterface arg0, int keyCode,
                                 KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    img_screenshots.setImageBitmap(null);
//                    Blurry.delete(constaint_main_view);
//                    revealShow(dialogView, false, dialog);
                    dialog.dismiss();
                }
                return true;
            }
        });
        dialog.getWindow().getDecorView().setBackgroundResource(android.R.color.transparent);
        dialog.getWindow().setDimAmount(0.0f);
        dialog.show();
        dialog.show();

    }


    private static int defaltValue = 0;

    private static boolean isVisible = false;
    long time1, time2;
    private TextView note = null;
    private View vInfo = null;

    private void showBottomShareDialog() {
        // Create custom dialog object

        final BottomSheetDialog dialog = new BottomSheetDialog(this);
        // Include dialog.xml file
        dialog.setContentView(R.layout.sharelocation_dialog);
        // Set dialog title
//        getWindow().setGravity(Gravity.BOTTOM);
//        getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        final TextView tv_time = dialog.findViewById(R.id.tv_time);
        Button btn_saveandshare = dialog.findViewById(R.id.btn_saveandshare);
        TextView tv_plus = dialog.findViewById(R.id.tv_plus);
        TextView tv_min = dialog.findViewById(R.id.tv_min);
        final ImageView chk_visible = dialog.findViewById(R.id.chk_visible);
        final ImageView chk_invisible = dialog.findViewById(R.id.chk_invisible);

        vInfo = getLayoutInflater().inflate(R.layout.info_window, null);


        if (isVisible) {
            chk_visible.setImageResource(R.drawable.remember_me_a);
            chk_invisible.setImageResource(R.drawable.remember_me_b);
        }

        btn_saveandshare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                utilClass.perfomroneClick();
                dialog.dismiss();
//                Blurry.delete(constaint_main_view);
                img_screenshots.setImageBitmap(null);
//                if (isVisible) {
                LatLng latLng2 = new LatLng(18.5989, 73.7653);
                if (markerPoints.size() >= 2)
                    markerPoints.clear();
                markerPoints.add(latLng2);

                options2 = new MarkerOptions();
                options2.position(latLng2);
                options2.title("pappapa");
                options2.icon(bitmapDescriptorFromVector(HomeActivity.this, R.drawable.defult_user));
                mMap.addMarker(options2);


                mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
                    // Use default InfoWindow frame
                    @Override
                    public View getInfoWindow(Marker marker) {
                        // Getting view from the layout file info_window_layout

                        if (marker.getTitle() != null) {

                            // Getting reference to the TextView to set title
                            note = vInfo.findViewById(R.id.note);

                            if (defaltValue != 0) {
                                note.setText(defaltValue + " min");
                                note.setVisibility(View.VISIBLE);
                            } else {
                                note.setVisibility(View.GONE);
                            }

                        } else {

                            // Getting reference to the TextView to set title
                            note = vInfo.findViewById(R.id.note);
                            note.setVisibility(View.GONE);
                        }
                        return vInfo;
                    }

                    // Defines the contents of the InfoWindow
                    @Override
                    public View getInfoContents(Marker marker) {

                        return null;
                    }
                });

                if (markerPoints != null && markerPoints.size() == 2)
                    homePresenter.callPolyLine((LatLng) markerPoints.get(0), (LatLng) markerPoints.get(1));


                time1 = defaltValue * 1000 * 60;
                time2 = defaltValue;
                countDownTimer = new CountDownTimer(time1, 60000) {

                    public void onTick(long millisUntilFinished) {
                        tv_timer_watch.setText(checkDigit(time2) + " min");
                        time2--;
                    }

                    public void onFinish() {
                        tv_timer_watch.setText("0 min");
                        defaltValue = 0;
                        isVisible = false;
                        note = vInfo.findViewById(R.id.note);
                        note.setText("");
                        note.setVisibility(View.GONE);
                        mMap.clear();

                        if (markerPoints.size() > 1)
                            markerPoints.remove(1);
                        if (markerPoints.size() >= 3)
                            markerPoints.remove(2);
                        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(markerPoints.get(0), 10);
                        mMap.animateCamera(cameraUpdate);


                        options = new MarkerOptions();
                        options.position(markerPoints.get(0));
                        if (sharePrefencesUtil.getUser() != null && sharePrefencesUtil.getUser().getPic() != null) {
                            addCustomMarkerFromURL();
                        } else {
                            options.icon(bitmapDescriptorFromVector(HomeActivity.this, R.drawable.defult_user));
                            mMap.addMarker(options);
                        }
                    }

                }.start();
//                }else{
//
//                }


            }
        });


        tv_plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ++defaltValue;
                String str = defaltValue + "";
                tv_time.setText(str);
            }
        });

        tv_min.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (defaltValue > 0) {
                    --defaltValue;
                }
                String str2 = defaltValue + "";
                tv_time.setText(str2);
            }
        });

        chk_visible.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isVisible) {
                    chk_visible.setImageResource(R.drawable.remember_me_a);
                    chk_invisible.setImageResource(R.drawable.remember_me_b);
                    isVisible = true;
                }
            }
        });

        chk_invisible.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isVisible) {
                    chk_visible.setImageResource(R.drawable.remember_me_b);
                    chk_invisible.setImageResource(R.drawable.remember_me_a);
                    isVisible = false;
                }
            }
        });


        int width = getResources().getDisplayMetrics().widthPixels;
        int height = getResources().getDisplayMetrics().heightPixels;
        dialog.getWindow().setLayout(width, height);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setOnKeyListener(new Dialog.OnKeyListener() {

            @Override
            public boolean onKey(DialogInterface arg0, int keyCode,
                                 KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
//                    Blurry.delete(constaint_main_view);
                    img_screenshots.setImageBitmap(null);
                    dialog.dismiss();
                }
                return true;
            }
        });

        dialog.getWindow().getDecorView().setBackgroundResource(android.R.color.transparent);
        dialog.getWindow().setDimAmount(0.0f);
        dialog.show();

        dialog.show();
    }

    public String checkDigit(long number) {
        return number <= 9 ? "0" + number : String.valueOf(number);
    }


    private void initView() {
        constaint_main_view = findViewById(R.id.constaint_main_view);
        mCustomMarkerView = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.view_custom_marker, null);
        profile_image = mCustomMarkerView.findViewById(R.id.profile_image);

        mainContainer = findViewById(R.id.mainContainer);
        tv_routeTime = findViewById(R.id.tv_routeTime);
        tv_routeTime.setOnClickListener(this);
        tv_timer_watch = findViewById(R.id.tv_timer_watch);
        txt_time = findViewById(R.id.txt_time);
        txt_temp = findViewById(R.id.txt_temp);
        txt_date = findViewById(R.id.txt_date);
        ll_uper = findViewById(R.id.ll_uper);

        img_1 = findViewById(R.id.img_1);
        img_1.setOnClickListener(this);
        img_2 = findViewById(R.id.img_2);
        img_2.setOnClickListener(this);
        img_3 = findViewById(R.id.img_3);
        img_3.setOnClickListener(this);
        img_box = findViewById(R.id.img_box);
        img_box.setOnClickListener(this);

        img_add_friend = findViewById(R.id.img_add_friend);
        img_add_friend.setOnClickListener(this);
        rv_friendList = findViewById(R.id.rv_friendList);
        layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);

        rv_friendList.setLayoutManager(layoutManager);

        friendArrayList.add(new Friend());
        friendArrayList.add(new Friend());
        friendArrayList.add(new Friend());
        friendArrayList.add(new Friend());
        friendAdapter = new FriendAdapter(this, friendArrayList, new OnClickerListener() {
            @Override
            public void onClick(int pos) {

            }
        });
        rv_friendList.setAdapter(friendAdapter);

        img_drive_mode = findViewById(R.id.img_drive_mode);
        img_drive_mode.setOnClickListener(this);
        img_contact_list = findViewById(R.id.img_contact_list);
        img_contact_list.setOnClickListener(this);
        img_user = findViewById(R.id.img_user);
        img_user.setOnClickListener(this);
        img_post = findViewById(R.id.img_post);
        img_post.setOnClickListener(this);
        img_route = findViewById(R.id.img_route);
        img_route.setOnClickListener(this);
        img_menu = findViewById(R.id.img_menu);
        img_menu.setOnClickListener(this);

        img_screen = findViewById(R.id.img_screen);
        img_screenshots = findViewById(R.id.img_screenshots);
        img_screen.setOnClickListener(this);

        txt_time.setText(UtilClass.getCurentDateandTime().split("and")[1]);
        txt_date.setText(UtilClass.getCurentDateandTime().split("and")[0]);


        //Record Video intialized
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        mScreenDensity = metrics.densityDpi;

        mMediaRecorder = new MediaRecorder();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mProjectionManager = (MediaProjectionManager) getSystemService
                    (Context.MEDIA_PROJECTION_SERVICE);
        }
        ll_videoView = findViewById(R.id.ll_videoView);
        img_play = findViewById(R.id.img_play);
        img_play.setOnClickListener(this);
        img_pause = findViewById(R.id.img_pause);
        img_pause.setOnClickListener(this);
        img_stop = findViewById(R.id.img_stop);
        img_stop.setOnClickListener(this);
//
        img_voice_record = findViewById(R.id.img_voice_record);
        img_voice_record.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                Intent intent = new Intent(Intent.ACTION_MAIN);
//                intent.setClassName("com.google.android.googlequicksearchbox",
//                        "com.google.android.googlequicksearchbox.VoiceSearchActivity");
//                try {
//                    startActivity(intent);
//                } catch (ActivityNotFoundException anfe) {
//                    Log.d(TAG, "Google Voice Search is not found");
//                }

                try {
                    showSpeachDialog();
                    startRecognition();
//                    recognitionProgressView.postDelayed(new Runnable() {
//                        @Override
//                        public void run() {
//                            startRecognition();
//                        }
//                    }, 50);

                } catch (Exception e) {
                    System.out.print("Come in exception");
                }
            }
        });
        img_video = findViewById(R.id.img_video);
        img_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ll_videoView.getVisibility() == View.INVISIBLE)
                    ll_videoView.setVisibility(View.VISIBLE);
                else
                    ll_videoView.setVisibility(View.INVISIBLE);
            }
        });


        mMediaProjectionCallback = new MediaProjectionCallback();
    }


    private void startScreenCapture() {
        //                ha = new Handler();
//                runnable = new Runnable() {
//
//                    @Override
//                    public void run() {
//                        //call function
//                        takeScreenShot();
//                        ha.postDelayed(this, 1000);
//                    }
//                };
//                ha.postDelayed(runnable, 1000);
        if (ContextCompat.checkSelfPermission(HomeActivity.this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE) + ContextCompat
                .checkSelfPermission(HomeActivity.this,
                        Manifest.permission.RECORD_AUDIO)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale
                    (HomeActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) ||
                    ActivityCompat.shouldShowRequestPermissionRationale
                            (HomeActivity.this, Manifest.permission.RECORD_AUDIO)) {
                isScreenCaptureRun = false;
                Snackbar.make(findViewById(android.R.id.content), "Permisson",
                        Snackbar.LENGTH_INDEFINITE).setAction("ENABLE",
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                ActivityCompat.requestPermissions(HomeActivity.this,
                                        new String[]{Manifest.permission
                                                .WRITE_EXTERNAL_STORAGE, Manifest.permission.RECORD_AUDIO},
                                        REQUEST_PERMISSIONS);
                            }
                        }).show();
            } else {
                ActivityCompat.requestPermissions(HomeActivity.this,
                        new String[]{Manifest.permission
                                .WRITE_EXTERNAL_STORAGE, Manifest.permission.RECORD_AUDIO},
                        REQUEST_PERMISSIONS);
            }
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                if (!isScreenCaptureRun) {
                    isScreenCaptureRun = true;
                    initRecorder();
                    shareScreen();
                    UtilClass.showBar(constaint_main_view, getString(R.string.play), this);
                }
            }
        }
    }

    private void stopScreenCapture() {
        //                ha.removeCallbacks(runnable);
//                showScreen();
        Log.v(TAG, "Stopping Recording");
        if (isScreenCaptureRun) {
            isScreenCaptureRun = false;
            stopScreenSharing();
            mMediaRecorder.stop();
            mMediaRecorder.reset();
            UtilClass.showBar(constaint_main_view, getString(R.string.stop), this);
            ll_videoView.setVisibility(View.GONE);
        }

    }


    private Runnable runnable1;

    private void showScreen() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final Handler handler = new Handler();
                runnable1 = new Runnable() {
                    int i = 0;

                    public void run() {
                        img_screenshots.setImageBitmap(bitmapArrayList.get(i));
                        i++;
                        if (i > bitmapArrayList.size() - 1) {
                            i = 0;
                        }

                        if (i >= bitmapArrayList.size())
                            handler.removeCallbacks(runnable1);
                        handler.postDelayed(this, 1000);
                    }
                };
                handler.postDelayed(runnable1, 1000);
            }
        });

    }

    private VirtualDisplay createVirtualDisplay() {
        return mMediaProjection.createVirtualDisplay("MainActivity",
                DISPLAY_WIDTH, DISPLAY_HEIGHT, mScreenDensity,
                DisplayManager.VIRTUAL_DISPLAY_FLAG_AUTO_MIRROR,
                mMediaRecorder.getSurface(), null /*Callbacks*/, null
                /*Handler*/);
    }

    private class MediaProjectionCallback extends MediaProjection.Callback {
        @Override
        public void onStop() {
            if (isScreenCaptureRun) {
                isScreenCaptureRun = false;
                mMediaRecorder.stop();
                mMediaRecorder.reset();
                Log.v(TAG, "Recording Stopped");
            }
            mMediaProjection = null;
            stopScreenSharing();
        }
    }


    private void shareScreen() {
        if (mMediaProjection == null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                startActivityForResult(mProjectionManager.createScreenCaptureIntent(), REQUEST_CODE);
            }
            return;
        }
        mVirtualDisplay = createVirtualDisplay();
        mMediaRecorder.start();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE) {
            mMediaProjectionCallback = new MediaProjectionCallback();
            mMediaProjection = mProjectionManager.getMediaProjection(resultCode, data);
            mMediaProjection.registerCallback(mMediaProjectionCallback, null);
            mVirtualDisplay = createVirtualDisplay();
            mMediaRecorder.start();
            return;
        } else if (resultCode != RESULT_OK) {
            Toast.makeText(this,
                    "Screen Cast Permission Denied", Toast.LENGTH_SHORT).show();
            isScreenCaptureRun = false;

            return;
        } else if (requestCode == REQ_CODE_SPEECH_INPUT) {
            if (resultCode == RESULT_OK && null != data) {

                ArrayList<String> result = data
                        .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                try {
//                    speechResponse(result);
                } catch (Exception e) {
                    System.out.print("Come in exception");
                }
            }
        } else {
            Log.e(TAG, "Unknown request code: " + requestCode);
            FragmentManager fm1 = getSupportFragmentManager();
            Fragment fragment = fm1.findFragmentByTag("settingDialogFragment");
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onInit(int status) {

        if (status == TextToSpeech.SUCCESS) {

            int result = tts.setLanguage(Locale.US);

            if (result == TextToSpeech.LANG_MISSING_DATA
                    || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                Log.e("TTS", "This Language is not supported");
            } else {
                imgbtn_speakknow.setEnabled(true);
                speakOut();
            }

        } else {
            Log.e("TTS", "Initilization Failed!");
        }

    }

    private void speakOut() {

        String text = txt_speech.getText().toString();

        tts.speak(text, TextToSpeech.QUEUE_FLUSH, null);
    }

    private void speechResponse(Bundle result) {
        ArrayList<String> matches = result
                .getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
        txt_speech.setText(matches.get(0));
//        recognitionProgressView.stop();
    }
    private void callSpeakIntent() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
                getString(R.string.speech_prompt));
        try {
            startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);
        } catch (ActivityNotFoundException a) {
            Toast.makeText(getApplicationContext(),
                    getString(R.string.speech_not_supported),
                    Toast.LENGTH_SHORT).show();
        }
    }

    private void startRecognition() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE, getPackageName());
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, "en");
        speechRecognizer.startListening(intent);
    }

    private void showSpeachDialog() {

        final Dialog dialog = new Dialog(this, R.style.dialog_slide_animation);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // Include dialog.xml file
        dialog.setContentView(R.layout.speech_dialog);
        txt_speech = dialog.findViewById(R.id.txt_speech);
        imgbtn_speakknow = dialog.findViewById(R.id.imgbtn_speakknow);
        int width = getResources().getDisplayMetrics().widthPixels;
        dialog.getWindow().setLayout(width - 100, ConstraintLayout.LayoutParams.WRAP_CONTENT);

        recognitionProgressView = dialog.findViewById(R.id.recognition_view);
        recognitionProgressView.setSpeechRecognizer(speechRecognizer);
        recognitionProgressView.setRecognitionListener(new RecognitionListenerAdapter() {
            @Override
            public void onResults(Bundle results) {
                speechResponse(results);
            }
        });
        int[] colors = {
                ContextCompat.getColor(this, R.color.color1),
                ContextCompat.getColor(this, R.color.color2),
                ContextCompat.getColor(this, R.color.color3),
                ContextCompat.getColor(this, R.color.color4),
                ContextCompat.getColor(this, R.color.color5)
        };

        recognitionProgressView.setColors(colors);
//        recognitionProgressView.setCircleRadiusInDp(2);
//        recognitionProgressView.setSpacingInDp(2);
//        recognitionProgressView.setIdleStateAmplitudeInDp(2);
//        recognitionProgressView.setRotationRadiusInDp(10);
        recognitionProgressView.play();

        tts = new TextToSpeech(this, this);
        imgbtn_speakknow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                speakOut();
            }
        });
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {
//                revealShow(dialogView, true, null);
            }
        });

        dialog.setOnKeyListener(new Dialog.OnKeyListener() {

            @Override
            public boolean onKey(DialogInterface arg0, int keyCode,
                                 KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    dialog.dismiss();
                }
                return true;
            }
        });
        dialog.show();
    }


    private void stopScreenSharing() {
        if (mVirtualDisplay == null) {
            return;
        }
        mVirtualDisplay.release();
        //mMediaRecorder.release(); //If used: mMediaRecorder object cannot
        // be reused again
        destroyMediaProjection();

    }

    private void destroyMediaProjection() {
        if (mMediaProjection != null) {
            mMediaProjection.unregisterCallback(mMediaProjectionCallback);
            mMediaProjection.stop();
            mMediaProjection = null;
        }
        Log.i(TAG, "MediaProjection Stopped");
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_PERMISSIONS: {
                if ((grantResults.length > 0) && (grantResults[0] +
                        grantResults[1]) == PackageManager.PERMISSION_GRANTED) {
                    isScreenCaptureRun = true;
                    initRecorder();
                    shareScreen();
                } else {
                    isScreenCaptureRun = false;
                    Snackbar.make(findViewById(android.R.id.content), "Permisson",
                            Snackbar.LENGTH_INDEFINITE).setAction("ENABLE",
                            new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent();
                                    intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                    intent.addCategory(Intent.CATEGORY_DEFAULT);
                                    intent.setData(Uri.parse("package:" + getPackageName()));
                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                                    startActivity(intent);
                                }
                            }).show();
                }
                return;
            }
        }
    }

    public String getFilePath() {
        final String directory = Environment.getExternalStorageDirectory() + File.separator + "Recordings";
        if (!Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
            Toast.makeText(this, "Failed to get External Storage", Toast.LENGTH_SHORT).show();
            return null;
        }
        final File folder = new File(directory);
        boolean success = true;
        if (!folder.exists()) {
            success = folder.mkdir();
        }
        String filePath;
        if (success) {
            String videoName = ("capture_" + getCurSysDate() + ".mp4");
            filePath = directory + File.separator + videoName;
        } else {
            Toast.makeText(this, "Failed to create Recordings directory", Toast.LENGTH_SHORT).show();
            return null;
        }
        return filePath;
    }

    public String getCurSysDate() {
        return new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss").format(new Date());
    }

    private void initRecorder() {
        try {
            mMediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
            mMediaRecorder.setVideoSource(MediaRecorder.VideoSource.SURFACE);
            mMediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
            mMediaRecorder.setOutputFile(Environment
                    .getExternalStoragePublicDirectory(Environment
                            .DIRECTORY_DOWNLOADS) + "/" + System.currentTimeMillis() + ".mp4");
            mMediaRecorder.setVideoSize(DISPLAY_WIDTH, DISPLAY_HEIGHT);
            mMediaRecorder.setVideoEncoder(MediaRecorder.VideoEncoder.H264);
            mMediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
            mMediaRecorder.setVideoEncodingBitRate(512 * 1000);
            mMediaRecorder.setVideoFrameRate(30);
            int rotation = getWindowManager().getDefaultDisplay().getRotation();
            int orientation = ORIENTATIONS.get(rotation + 90);
            mMediaRecorder.setOrientationHint(orientation);
            mMediaRecorder.prepare();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.clear();
        // Add a marker in Sydney and move the camera
//        LatLng sydney = new LatLng(-34, 151);
//        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }


        googleMap.setMyLocationEnabled(true);

        // Getting LocationManager object from System Service LOCATION_SERVICE
//        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        // Creating a criteria object to retrieve provider
//        Criteria criteria = new Criteria();

        // Getting the name of the best provider
//        String provider = locationManager.getBestProvider(criteria, true);

        // Getting Current Location
//        Location location = locationManager.getLastKnownLocation(provider);

//        if (location != null) {
//            onLocationChanged(location);
//        }
//        locationManager.requestLocationUpdates(provider, 200, 0, this);


    }

    private ArrayList<Bitmap> bitmapArrayList = new ArrayList<>();

    public void takeScreenShot() {
        bitmapArrayList.clear();
        GoogleMap.SnapshotReadyCallback callback = new GoogleMap.SnapshotReadyCallback() {
            @Override
            public void onSnapshotReady(Bitmap snapshot) {

                try {
                    constaint_main_view.setDrawingCacheEnabled(true);
                    Bitmap backBitmap = constaint_main_view.getDrawingCache();
                    Bitmap bmOverlay = Bitmap.createBitmap(
                            backBitmap.getWidth(), backBitmap.getHeight(),
                            backBitmap.getConfig());
                    Canvas canvas = new Canvas(bmOverlay);
                    canvas.drawBitmap(snapshot, new Matrix(), null);
                    canvas.drawBitmap(backBitmap, 0, 0, null);
                    // image naming and path  to include sd card  appending name you choose for file
//                    String mPath = Environment.getExternalStorageDirectory().toString() + "Goho/Screenshots/" + System.currentTimeMillis() + ".png";
//                    File fileDir = new File(Environment.getExternalStorageDirectory(), "GOHO");
//                    if (!fileDir.exists())
//                        fileDir.mkdirs();

//                    String path = "/GOHO_Screen_"
//                            + System.currentTimeMillis() + ".png";
//                    File file = new File(fileDir, path);

//                    FileOutputStream out = new FileOutputStream(file);
//                    bmOverlay.compress(Bitmap.CompressFormat.PNG, 90, out);
                    bitmapArrayList.add(bmOverlay);
//                    out.flush();
//                    out.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };

        mMap.snapshot(callback);
    }

    private BitmapDescriptor bitmapDescriptorFromVector(Context context,
                                                        @DrawableRes int vectorDrawableResourceId) {
        Drawable background = ContextCompat.getDrawable(context, R.drawable.my_location);
        background.setBounds(0, 0, background.getIntrinsicWidth(), background.getIntrinsicHeight());
        Drawable vectorDrawable = ContextCompat.getDrawable(context, vectorDrawableResourceId);
        vectorDrawable.setBounds(0, 0, vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight());
        Bitmap bitmap = Bitmap.createBitmap(background.getIntrinsicWidth(), background.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);


        Canvas canvas = new Canvas(bitmap);
        background.draw(canvas);
        vectorDrawable.draw(canvas);

        return BitmapDescriptorFactory.fromBitmap(bitmap);

    }


////    @Override
//    public void onLocationChanged(Location location) {
//        try {
//            latLng = new LatLng(location.getLatitude(), location.getLongitude());
//            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 10);
//            mMap.animateCamera(cameraUpdate);
////            locationManager.removeUpdates(this);
//
//            markerPoints.add(latLng);
////            LatLng latLng2 = new LatLng(18.5989, 73.7653);
////            markerPoints.add(latLng2);
//
//
//            options = new MarkerOptions();
//            options.position(latLng);
////        bitmapDescriptorFromVector(this, R.drawable.defult_user);
////            URL url = new URL(sharePrefencesUtil.getUser().getPic());
////            Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());
////            options.icon(BitmapDescriptorFactory.fromBitmap(bmp));
////            mMap.addMarker(options);
//            if (sharePrefencesUtil.getUser() != null && sharePrefencesUtil.getUser().getPic() != null) {
//                addCustomMarkerFromURL();
//            } else {
//                options.icon(bitmapDescriptorFromVector(this, R.drawable.defult_user));
//                mMap.addMarker(options);
//            }
//
//            new HomePresenter.WeatherAsyn(HomeActivity.this,this,location.getLatitude(),location.getLongitude()).execute();
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//
////            options2 = new MarkerOptions();
////            options2.position(latLng2);
////        bitmapDescriptorFromVector(this, R.drawable.defult_user);
////            options2.icon(bitmapDescriptorFromVector(this, R.drawable.defult_user));
////            mMap.addMarker(options2);
//    }
//
//    @Override
//    public void onStatusChanged(String provider, int status, Bundle extras) {
//    }
//
//    @Override
//    public void onProviderEnabled(String provider) {
//    }
//
//    @Override
//    public void onProviderDisabled(String provider) {
//    }


    private void addCustomMarkerFromURL() {

        if (mMap == null) {
            return;
        }
        // adding a marker with image from URL using glide image loading library
        Glide.with(getApplicationContext())
                .load(sharePrefencesUtil.getUser().getPic()).asBitmap().fitCenter()
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(Bitmap bitmap, GlideAnimation<? super Bitmap> glideAnimation) {

                        mMap.addMarker(new MarkerOptions().position(latLng)
                                .icon(BitmapDescriptorFactory.fromBitmap(getMarkerBitmapFromView(mCustomMarkerView, bitmap))));

                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 13f));
                        if (isVisible && markerPoints.size() == 3) {
                            ployLine.remove();
                            homePresenter.callPolyLine((LatLng) markerPoints.get(2), (LatLng) markerPoints.get(1));
                        }

                    }
                });
    }


    private Bitmap getMarkerBitmapFromView(View view, Bitmap bitmap) {

        profile_image.setImageBitmap(bitmap);
        view.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());
        view.buildDrawingCache();
        Bitmap returnedBitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(),
                Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(returnedBitmap);
        canvas.drawColor(Color.WHITE, PorterDuff.Mode.SRC_IN);
        Drawable drawable = view.getBackground();
        if (drawable != null)
            drawable.draw(canvas);
        view.draw(canvas);
        return returnedBitmap;

    }


    private float xCurrentPos, yCurrentPos;

    @Override
    public void onClick(View v) {
        utilClass.perfomroneClick();
        switch (v.getId()) {
            case R.id.tv_routeTime:
                if (!iswatchClick) {
                    tv_timer_watch.setVisibility(View.VISIBLE);
                    iswatchClick = true;
                } else {
                    tv_timer_watch.setVisibility(View.GONE);
                    iswatchClick = false;
                }
                break;
            case R.id.img_screen:
                try {
                    utilClass.takeScreenShot(constaint_main_view, mMap, true, new BlurScreenCallback() {
                        @Override
                        public void onShowBitmap(Bitmap bitmap) {
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;
            case R.id.img_1:
                Toast.makeText(HomeActivity.this, "Under Development", Toast.LENGTH_LONG).show();
                FragmentManager fm1 = getSupportFragmentManager();
                SettingDialogFragment settingDialogFragment = new SettingDialogFragment();
                settingDialogFragment.setCallBack(this);
                settingDialogFragment.show(fm1, "settingDialogFragment");

                try {
                    utilClass.takeScreenShot(constaint_main_view, mMap, false, new BlurScreenCallback() {
                        @Override
                        public void onShowBitmap(Bitmap bitmap) {
                            img_screenshots.setImageBitmap(bitmap);
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.img_2:
                Toast.makeText(HomeActivity.this, "Under Development", Toast.LENGTH_LONG).show();
                break;
            case R.id.img_3:
                Toast.makeText(HomeActivity.this, "Under Development", Toast.LENGTH_LONG).show();
                break;
            case R.id.img_drive_mode:
                Toast.makeText(HomeActivity.this, "Under Development", Toast.LENGTH_LONG).show();
                break;
            case R.id.img_contact_list:
                FragmentManager fm = getSupportFragmentManager();
                ContactListFragment contactListFragment = new ContactListFragment();
                Bundle bundle = new Bundle();
                bundle.putParcelableArrayList("contact", contactArrayList);
                contactListFragment.setArguments(bundle);
                contactListFragment.show(fm, "dialog");
                try {
                    utilClass.takeScreenShot(constaint_main_view, mMap, false, new BlurScreenCallback() {
                        @Override
                        public void onShowBitmap(Bitmap bitmap) {
                            img_screenshots.setImageBitmap(bitmap);
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.img_route:
                Toast.makeText(HomeActivity.this, "Under Development", Toast.LENGTH_LONG).show();
                break;
            case R.id.img_post:
                Toast.makeText(HomeActivity.this, "Under Development", Toast.LENGTH_LONG).show();
                FragmentManager fm2 = getSupportFragmentManager();
                PostDialogFragment postDialogFragment = new PostDialogFragment();
                postDialogFragment.setCallBack(this);
                postDialogFragment.show(fm2, "dialog");

                try {
                    utilClass.takeScreenShot(constaint_main_view, mMap, false, new BlurScreenCallback() {
                        @Override
                        public void onShowBitmap(Bitmap bitmap) {
                            img_screenshots.setImageBitmap(bitmap);
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.img_user:
                Toast.makeText(HomeActivity.this, "Under Development", Toast.LENGTH_LONG).show();
                break;
            case R.id.img_box:
                callBoxAnimation();
                break;
            case R.id.img_menu:

                callMenuAnimation();

                break;
            case R.id.img_add_friend:
                FragmentManager fm3 = getSupportFragmentManager();

                GroupGohoFragment groupGohoFragment = new GroupGohoFragment();
                groupGohoFragment.setCallBack(this);
                Bundle bundle2 = new Bundle();
                bundle2.putParcelableArrayList("group", groupArrayList);
                bundle2.putParcelableArrayList("contact", contactArrayList);
                groupGohoFragment.setArguments(bundle2);
                groupGohoFragment.show(fm3, "dialog");

                try {
                    utilClass.takeScreenShot(constaint_main_view, mMap, false, new BlurScreenCallback() {
                        @Override
                        public void onShowBitmap(Bitmap bitmap) {
                            img_screenshots.setImageBitmap(bitmap);
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
//                Blurry.with(HomeActivity.this)
//                        .radius(25)
////                .sampling(2)
//                        .color(Color.argb(50, 100, 100, 100))
//                        .async()
//                        .onto(constaint_main_view);
//                onback();
                break;
            case R.id.img_play:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    startScreenCapture();
                }
                break;
            case R.id.img_pause:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    if (isScreenCaptureRun) {
                        mMediaRecorder.pause();
                        UtilClass.showBar(constaint_main_view, getString(R.string.pause), this);
                    }
                }
                break;
            case R.id.img_stop:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    stopScreenCapture();
                }
                break;
        }
    }

    private void callMenuAnimation() {
        //                xCurrentPos = img_post.getLeft();
//                yCurrentPos = img_post.getTop();

        final int newleft = img_post.getMeasuredWidth();
//                final int newTop = img_post.getTop() - 100;
        Toast.makeText(HomeActivity.this, "Under Development", Toast.LENGTH_LONG).show();


//                int buttonHeight = img_post.getHeight();
//                int parentHeight = mainContainer.getHeight();
//                final int buttonWidth = img_post.getWidth();
//                int parentWidth = mainContainer.getWidth();

//                boolean isInCenter = img_post.getLeft() > 0;
//                final int fromX = isInCenter ? (parentWidth / 2) - (buttonWidth / 2) : 0;
//                final int fromY = isInCenter?(parentHeight/2)-(buttonHeight/2):0;
//                final int toX = isInCenter ? 0 : (parentWidth / 2) - (buttonWidth / 2);
//                final int toY = isInCenter?0:(parentHeight/2)-(buttonHeight/2);


//                AnimationSet animationSet = new AnimationSet(true);
        TranslateAnimation translate = null, translate2 = null, translate3 = null,
                translate4 = null, translate5 = null;
        if (!isOpen) {
            translate = new TranslateAnimation(0, newleft + (newleft / 6), 0, 0);
            translate2 = new TranslateAnimation(0, (2 * newleft) + (newleft / 3), 0, 0);
            translate3 = new TranslateAnimation(0, -(newleft + (newleft / 6)), 0, 0);
            translate4 = new TranslateAnimation(0, (-((2 * newleft) + newleft / 3)), 0, 0);
            translate5 = new TranslateAnimation(0, (-((3 * newleft) + newleft / 2)), 0, 0);
        } else {
            translate = new TranslateAnimation(0, (-(newleft + (newleft / 6))), 0, 0);
            translate2 = new TranslateAnimation(0, (-((2 * newleft) + (newleft / 3))), 0, 0);
            translate3 = new TranslateAnimation(0, (newleft + (newleft / 6)), 0, 0);
            translate4 = new TranslateAnimation(0, (((2 * newleft) + newleft / 3)), 0, 0);
            translate5 = new TranslateAnimation(0, (((3 * newleft) + newleft / 2)), 0, 0);
        }
        translate.setDuration(1000);
        translate.setFillAfter(true);
        translate2.setDuration(1000);
        translate2.setFillAfter(true);
        translate3.setDuration(1000);
        translate3.setFillAfter(true);
        translate4.setDuration(1000);
        translate4.setFillAfter(true);
        translate5.setDuration(1000);
        translate5.setFillAfter(true);
        translate5.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (!isOpen) {
                    //when the animation ended the button is really moved, to remain clickable
                    RelativeLayout.LayoutParams params2 = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                    params2.leftMargin = (img_post.getLeft()) + (newleft + (newleft / 6));
                    img_post.setLayoutParams(params2);
                    //remove the animation, its no longer needed, since button is really there
                    img_post.clearAnimation();

                    RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                    params.leftMargin = (img_user.getLeft()) + (2 * newleft) + (newleft / 3);
                    img_user.setLayoutParams(params);
                    //remove the animation, its no longer needed, since button is really there
                    img_user.clearAnimation();

                    RelativeLayout.LayoutParams params3 = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                    params3.leftMargin = (img_route.getLeft()) + (-(newleft + (newleft / 6)));
                    img_route.setLayoutParams(params3);
                    //remove the animation, its no longer needed, since button is really there
                    img_route.clearAnimation();

                    RelativeLayout.LayoutParams params4 = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                    params4.leftMargin = (img_contact_list.getLeft()) + ((-((2 * newleft) + newleft / 3)));
                    img_contact_list.setLayoutParams(params4);
                    //remove the animation, its no longer needed, since button is really there
                    img_contact_list.clearAnimation();

                    RelativeLayout.LayoutParams params5 = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                    params5.leftMargin = (img_drive_mode.getLeft()) + ((-((3 * newleft) + newleft / 2)));
                    img_drive_mode.setLayoutParams(params5);
                    //remove the animation, its no longer needed, since button is really there
                    img_drive_mode.clearAnimation();
                } else {
                    //when the animation ended the button is really moved, to remain clickable

                    RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                    params.leftMargin = img_menu.getLeft();
                    img_user.setLayoutParams(params);
                    img_user.clearAnimation();

                    RelativeLayout.LayoutParams params2 = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                    params2.leftMargin = img_menu.getLeft();
                    img_post.setLayoutParams(params2);
                    //remove the animation, its no longer needed, since button is really there
                    img_post.clearAnimation();
                    //remove the animation, its no longer needed, since button is really there
//                            img_user.clearAnimation();


                    RelativeLayout.LayoutParams params3 = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                    params3.leftMargin = img_menu.getLeft();
                    img_route.setLayoutParams(params3);
                    //remove the animation, its no longer needed, since button is really there
                    img_route.clearAnimation();

                    RelativeLayout.LayoutParams params4 = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                    params4.leftMargin = img_menu.getLeft();
                    img_contact_list.setLayoutParams(params4);
                    //remove the animation, its no longer needed, since button is really there
                    img_contact_list.clearAnimation();

                    RelativeLayout.LayoutParams param5 = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                    param5.leftMargin = img_menu.getLeft();
                    img_drive_mode.setLayoutParams(param5);
                    //remove the animation, its no longer needed, since button is really there
                    img_drive_mode.clearAnimation();
                }
                if (!isOpen)
                    isOpen = true;
                else
                    isOpen = false;
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

        });

        img_post.startAnimation(translate);
        img_user.startAnimation(translate2);
        img_route.startAnimation(translate3);
        img_contact_list.startAnimation(translate4);
        img_drive_mode.startAnimation(translate5);


    }


    private void callBoxAnimation() {
        TranslateAnimation translate11 = null, translate22 = null, translate33 = null;
        final int newleft2 = img_1.getMeasuredWidth();
        if (!isBoxOpen) {
            translate11 = new TranslateAnimation(0, newleft2 + (newleft2 / 6), 0, 0);
            translate22 = new TranslateAnimation(0, (2 * newleft2) + (newleft2 / 3), 0, 0);
            translate33 = new TranslateAnimation(0, (((3 * newleft2) + newleft2 / 2)), 0, 0);
        } else {
            translate11 = new TranslateAnimation(0, (-(newleft2 + (newleft2 / 6))), 0, 0);
            translate22 = new TranslateAnimation(0, (-((2 * newleft2) + (newleft2 / 3))), 0, 0);
            translate33 = new TranslateAnimation(0, (-((3 * newleft2) + newleft2 / 2)), 0, 0);
        }

        translate11.setDuration(1000);
        translate11.setFillAfter(true);
        translate22.setDuration(1000);
        translate22.setFillAfter(true);
        translate33.setDuration(1000);
        translate33.setFillAfter(true);
        translate33.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                if (!isBoxOpen)
                    ll_uper.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (!isBoxOpen) {
                    //when the animation ended the button is really moved, to remain clickable
                    RelativeLayout.LayoutParams params2 = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                    params2.leftMargin = (img_1.getLeft()) + (newleft2 + (newleft2 / 6));
                    img_1.setLayoutParams(params2);
                    //remove the animation, its no longer needed, since button is really there
                    img_1.clearAnimation();

                    RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                    params.leftMargin = (img_2.getLeft()) + (2 * newleft2) + (newleft2 / 3);
                    img_2.setLayoutParams(params);
                    //remove the animation, its no longer needed, since button is really there
                    img_2.clearAnimation();

                    RelativeLayout.LayoutParams params3 = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                    params3.leftMargin = (img_3.getLeft()) + (3 * newleft2) + (newleft2 / 2);
                    img_3.setLayoutParams(params3);
                    //remove the animation, its no longer needed, since button is really there
                    img_3.clearAnimation();

                } else {
                    //when the animation ended the button is really moved, to remain clickable

                    RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                    params.leftMargin = img_box.getLeft();
                    img_1.setLayoutParams(params);
                    img_1.clearAnimation();

                    RelativeLayout.LayoutParams params2 = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                    params2.leftMargin = img_box.getLeft();
                    img_2.setLayoutParams(params2);
                    //remove the animation, its no longer needed, since button is really there
                    img_2.clearAnimation();

                    RelativeLayout.LayoutParams params3 = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                    params3.leftMargin = img_box.getLeft();
                    img_3.setLayoutParams(params3);
                    //remove the animation, its no longer needed, since button is really there
                    img_3.clearAnimation();

                }
                if (!isBoxOpen) {
                    isBoxOpen = true;
                } else {
                    isBoxOpen = false;
                    ll_uper.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        img_1.startAnimation(translate11);
        img_2.startAnimation(translate22);
        img_3.startAnimation(translate33);
    }

    private ArrayList<Group> groupArrayList = new ArrayList<>();
    private ArrayList<Friend> contactArrayList = new ArrayList<>();

    private boolean loadData() {
        for (int i = 0; i < 5; i++) {
            Group group = new Group();
            ArrayList<Friend> friendArrayList = new ArrayList<>();
            friendArrayList.add(new Friend());
            friendArrayList.add(new Friend());
            friendArrayList.add(new Friend());
            group.setFriendArrayList(friendArrayList);
            groupArrayList.add(group);
        }

        for (int i = 0; i < 10; i++) {
            contactArrayList.add(new Friend());
        }

        return true;
    }


    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onBackTemp(double temp) {
        txt_temp.setText(((int) temp) + "\u00b0" + "C");
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        if (resultData.containsKey("location")) {
            Location location = resultData.getParcelable("location");
            try {

                if (markerPoints.size() == 0 || markerPoints.size() == 1) {
                    mMap.clear();
                    latLng = new LatLng(location.getLatitude(), location.getLongitude());
                    CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 10);
                    mMap.animateCamera(cameraUpdate);
                    markerPoints.clear();
                    markerPoints.add(latLng);

                    options = new MarkerOptions();
                    options.position(latLng);
                    if (sharePrefencesUtil.getUser() != null && sharePrefencesUtil.getUser().getPic() != null) {
                        addCustomMarkerFromURL();
                    } else {
                        options.icon(bitmapDescriptorFromVector(this, R.drawable.defult_user));
                        mMap.addMarker(options);
                    }
                } else {
                    if (isVisible) {
                        latLng = new LatLng(location.getLatitude(), location.getLongitude());
                        if (markerPoints.size() >= 3)
                            markerPoints.remove(2);
                        markerPoints.add(latLng);

                        options3 = new MarkerOptions();
                        options3.position(latLng);

                        if (sharePrefencesUtil.getUser() != null && sharePrefencesUtil.getUser().getPic() != null) {
                            addCustomMarkerFromURL();
                        } else {
                            options.icon(bitmapDescriptorFromVector(this, R.drawable.defult_user));
                            mMap.addMarker(options3);
                        }
                    } else {

                    }
                }
                new HomePresenter.WeatherAsyn(HomeActivity.this, this, location.getLatitude(), location.getLongitude()).execute();

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            UtilClass.showAlertDialogButtonClicked(HomeActivity.this, new OnGPSChooseListener() {
                @Override
                public void onchoose() {
                    startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    stopService(gpsIntent);
                    gpsIntent = null;
                }
            });
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopService(gpsIntent);
        destroyMediaProjection();
        if (tts != null) {
            tts.stop();
            tts.shutdown();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }


}
