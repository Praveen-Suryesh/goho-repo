package com.inpetrosys.goho.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.inpetrosys.goho.R;
import com.inpetrosys.goho.fragment.login.LoginFragment;

public class StartActivity extends AppCompatActivity {
    FragmentTransaction ft;
    FragmentManager fm;
    private FragmentManager manager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        Window window = getWindow();

//// clear FLAG_TRANSLUCENT_STATUS flag:
//        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
//
//// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
//        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
//
//// finally change the color
//        window.setStatusBarColor(ContextCompat.getColor(this,R.color.white));
        fm=getSupportFragmentManager();
//        ft = fm.beginTransaction();
//        LoginFragment loginFragment = new LoginFragment();
//        ft.add(R.id.frame_content, loginFragment);
//        ft.commit();
        ft=getSupportFragmentManager().beginTransaction();
        LoginFragment loginFragment=new LoginFragment();
        ft.add(R.id.frame_content,loginFragment,"login").commit();
//        replaceFragment(loginFragment);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Fragment fragment = fm.findFragmentById(R.id.frame_content);
        fragment.onActivityResult(requestCode, resultCode, data);
    }

    public void backstackImmidiate(Fragment fragment) {
        String backStateName = fragment.getClass().getName();

        FragmentManager manager = getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

//        if (!fragmentPopped) { //fragment not in back stack, create it.
//            FragmentTransaction ft = manager.beginTransaction();
//            ft.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_in_right, R.anim.slide_out_left);
//            ft.replace(R.id.frame_content, fragment);
//            ft.addToBackStack(backStateName);
//            ft.commit();
//        }
    }

    public void replaceFragment(Fragment fragment) {
        String backStateName = fragment.getClass().getName();

        manager = getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

        if (!fragmentPopped) { //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_in_right, R.anim.slide_out_left);
            ft.replace(R.id.frame_content, fragment);
            ft.addToBackStack(backStateName);
            ft.commit();
        }
    }
    public void clearStack(){
        manager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        if(manager.getBackStackEntryCount()==0){
//            clearStack();
//            finish();
//        }

    }
}
