package com.inpetrosys.goho.customview;

import android.content.Context;
import android.graphics.Typeface;

public class TypeFactory {

    private String T_BLACK= "TitilliumWeb-Black.ttf";
    private String T_BOLD="TitilliumWeb-Bold.ttf";
    private String T_BOLDITALIC= "TitilliumWeb-BoldItalic.ttf";
    private String T_EXTRALIGHT= "TitilliumWeb-ExtraLight.ttf";
    private String T_EXTRALIGHTITALIC="TitilliumWeb-ExtraLightItalic.ttf";
    private String T_ITALIC="TitilliumWeb-Italic.ttf";
    private String T_LIGHT="TitilliumWeb-Light.ttf";
    private String T_LIGHTITALIC="TitilliumWeb-LightItalic.ttf";
    private String T_REGULAR="TitilliumWeb-Regular.ttf";
    private String T_SEMIBOLD="TitilliumWeb-SemiBold.ttf";
    private String T_SEMIBOLDITALIC="TitilliumWeb-SemiBoldItalic.ttf";

    Typeface TitilliumWeb_Black;
    Typeface TitilliumWeb_Bold;
    Typeface TitilliumWeb_BoldItalic;
    Typeface TitilliumWeb_ExtraLight;
    Typeface TitilliumWeb_ExtraLightItalic;
    Typeface TitilliumWeb_Italic;
    Typeface TitilliumWeb_Light;
    Typeface TitilliumWeb_LightItalic;
    Typeface TitilliumWeb_Regular;
    Typeface TitilliumWeb_SemiBold;
    Typeface TitilliumWeb_SemiBoldItalic;

    public TypeFactory(Context context){
        TitilliumWeb_Black = Typeface.createFromAsset(context.getAssets(),T_BLACK);
        TitilliumWeb_Bold = Typeface.createFromAsset(context.getAssets(),T_BOLD);
        TitilliumWeb_BoldItalic = Typeface.createFromAsset(context.getAssets(),T_BOLDITALIC);
        TitilliumWeb_ExtraLight = Typeface.createFromAsset(context.getAssets(),T_EXTRALIGHT);
        TitilliumWeb_ExtraLightItalic = Typeface.createFromAsset(context.getAssets(),T_EXTRALIGHTITALIC);
        TitilliumWeb_Italic = Typeface.createFromAsset(context.getAssets(),T_ITALIC);
        TitilliumWeb_Light = Typeface.createFromAsset(context.getAssets(),T_LIGHT);
        TitilliumWeb_LightItalic = Typeface.createFromAsset(context.getAssets(),T_LIGHTITALIC);
        TitilliumWeb_Regular = Typeface.createFromAsset(context.getAssets(),T_REGULAR);
        TitilliumWeb_SemiBold = Typeface.createFromAsset(context.getAssets(),T_SEMIBOLD);
        TitilliumWeb_SemiBoldItalic = Typeface.createFromAsset(context.getAssets(),T_SEMIBOLDITALIC);
    }

}
