package com.inpetrosys.goho.customview;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import com.inpetrosys.goho.R;

public class CustomTextView extends android.support.v7.widget.AppCompatTextView {

    private int typefaceType;
    private TypeFactory mFontFactory;

    public CustomTextView(Context context, AttributeSet attrs) {
        super(context, attrs);

        applyCustomFont(context, attrs);
    }

    public CustomTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        applyCustomFont(context, attrs);
    }

    public CustomTextView(Context context) {
        super(context);
    }

    private void applyCustomFont(Context context, AttributeSet attrs) {


        TypedArray array = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.CustomTextView,
                0, 0);
        try {
            typefaceType = array.getInteger(R.styleable.CustomTextView_font_name, 0);
        } finally {
            array.recycle();
        }
        if (!isInEditMode()) {
            setTypeface(getTypeFace(typefaceType));
        }

    }

    public Typeface getTypeFace(int type) {
        if (mFontFactory == null)
            mFontFactory = new TypeFactory(getContext());

        switch (type) {
            case Constants.T_BLACK:
                return mFontFactory.TitilliumWeb_Black;

            case Constants.T_BOLD:
                return mFontFactory.TitilliumWeb_Bold;

            case Constants.T_BOLDITALIC:
                return mFontFactory.TitilliumWeb_BoldItalic;

            case Constants.T_EXTRALIGHT:
                return mFontFactory.TitilliumWeb_ExtraLight;

            case Constants.T_EXTRALIGHTITALIC:
                return mFontFactory.TitilliumWeb_ExtraLightItalic;
            case Constants.T_ITALIC:
                return mFontFactory.TitilliumWeb_Italic;
            case Constants.T_LIGHT:
                return mFontFactory.TitilliumWeb_Light;
            case Constants.T_LIGHTITALIC:
                return mFontFactory.TitilliumWeb_LightItalic;
            case Constants.T_REGULAR:
                return mFontFactory.TitilliumWeb_Regular;
            case Constants.T_SEMIBOLD:
                return mFontFactory.TitilliumWeb_SemiBold;
            case Constants.T_SEMIBOLDITALIC:
                return mFontFactory.TitilliumWeb_SemiBoldItalic;

            default:
                return mFontFactory.TitilliumWeb_Regular;
        }
    }

    public interface Constants {
        int T_BLACK = 1,
                T_BOLD = 2,
                T_BOLDITALIC = 3,
                T_EXTRALIGHT = 4,
                T_EXTRALIGHTITALIC = 5,
                T_ITALIC = 6,
                T_LIGHT = 7,
                T_LIGHTITALIC = 8,
                T_REGULAR = 9,
                T_SEMIBOLD = 10,
                T_SEMIBOLDITALIC = 11;
    }


}
