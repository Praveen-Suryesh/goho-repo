package com.inpetrosys.goho.beans;

import android.os.Parcel;
import android.os.Parcelable;

public class Friend implements Parcelable {
    private String name;
    private boolean isChk;

    protected Friend(Parcel in) {
        name = in.readString();
    }
    public Friend(){}

    public static final Creator<Friend> CREATOR = new Creator<Friend>() {
        @Override
        public Friend createFromParcel(Parcel in) {
            return new Friend(in);
        }

        @Override
        public Friend[] newArray(int size) {
            return new Friend[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
    }

    public boolean isChk() {
        return isChk;
    }

    public void setChk(boolean chk) {
        isChk = chk;
    }
}
