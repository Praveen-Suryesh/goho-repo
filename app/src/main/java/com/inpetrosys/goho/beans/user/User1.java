package com.inpetrosys.goho.beans.user;

import com.google.gson.annotations.SerializedName;

public class User1 {

    @SerializedName("name")
    public String name;
    @SerializedName("job")
    public String job;
    @SerializedName("id")
    public String id;
    @SerializedName("createdAt")
    public String createdAt;

    public User1(String name, String job) {
        this.name = name;
        this.job = job;
    }


}