package com.inpetrosys.goho.beans;

import android.os.Parcel;
import android.os.Parcelable;

public class User implements Parcelable{
    public String mobileNumber;
    public String birthDay;
    public String name;
    public String pic;

    public User(Parcel in) {
        setMobileNumber(in.readString());
        setBirthDay(in.readString());
        setName(in.readString());
        setPic(in.readString());
    }
    public User(){}

    private static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    public static Creator<User> getCREATOR() {
        return CREATOR;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(getMobileNumber());
        dest.writeString(getBirthDay());
        dest.writeString(getName());
        dest.writeString(getPic());
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(String birthDay) {
        this.birthDay = birthDay;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }
}
