package com.inpetrosys.goho.beans;

public class Post {
    private String postImg;
    private String postName;
    private String postDes;
    private boolean isEdit;
    private String post_location1;
    private String post_location2;
    private boolean islocation1;
    private boolean islocation2;

    public String getPostImg() {
        return postImg;
    }

    public void setPostImg(String postImg) {
        this.postImg = postImg;
    }

    public String getPostName() {
        return postName;
    }

    public void setPostName(String postName) {
        this.postName = postName;
    }

    public String getPostDes() {
        return postDes;
    }

    public void setPostDes(String postDes) {
        this.postDes = postDes;
    }

    public boolean isEdit() {
        return isEdit;
    }

    public void setEdit(boolean edit) {
        isEdit = edit;
    }

    public String getPost_location1() {
        return post_location1;
    }

    public void setPost_location1(String post_location1) {
        this.post_location1 = post_location1;
    }

    public String getPost_location2() {
        return post_location2;
    }

    public void setPost_location2(String post_location2) {
        this.post_location2 = post_location2;
    }

    public boolean isIslocation1() {
        return islocation1;
    }

    public void setIslocation1(boolean islocation1) {
        this.islocation1 = islocation1;
    }

    public boolean isIslocation2() {
        return islocation2;
    }

    public void setIslocation2(boolean islocation2) {
        this.islocation2 = islocation2;
    }
}
