package com.inpetrosys.goho.Interfaceuser;

import android.graphics.Bitmap;

public interface BlurScreenCallback {
    void onShowBitmap(Bitmap bitmap);
}
