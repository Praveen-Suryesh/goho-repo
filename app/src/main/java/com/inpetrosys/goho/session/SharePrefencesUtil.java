package com.inpetrosys.goho.session;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.inpetrosys.goho.beans.User;

public class SharePrefencesUtil {
    private SharedPreferences sharedPreferences;
    public static final String MyPREFERENCES = "MyPrefs";

    public SharePrefencesUtil(Context context) {
        sharedPreferences = context.getSharedPreferences(MyPREFERENCES,
                Context.MODE_PRIVATE);
    }

    public void saveUser(User user) {
        SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(user);
        prefsEditor.putString("user", json);
        prefsEditor.commit();
    }

    public User getUser() {
        Gson gson = new Gson();
        String json = sharedPreferences.getString("user", "");
        User user = gson.fromJson(json, User.class);
        return user;
    }

    public void setString(String value, String key) {
        SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
        prefsEditor.putString(key, value);
        prefsEditor.commit();
    }

    public String getString(String key) {
        String name = "";
        String restoredText = sharedPreferences.getString(key, null);
        if (restoredText != null) {
            name = sharedPreferences.getString(key, "");
        }
        return name;
    }

    public void setBoolean(boolean value, String key) {
        SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
        prefsEditor.putBoolean(key, value);
        prefsEditor.commit();
    }

    public boolean getBoolean(String key) {
        boolean name;
//        boolean restoredText = sharedPreferences.getBoolean(key, false);
        name = sharedPreferences.getBoolean(key, false);
        return name;
    }

}
