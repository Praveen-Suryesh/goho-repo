package com.inpetrosys.goho.apicalling;

import com.inpetrosys.goho.beans.Main;
import com.inpetrosys.goho.beans.user.User1;
import com.inpetrosys.goho.beans.user.UserList;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface APIInterface {

    @GET("/data/2.5/weather?")
    Call<Main> doGetListResources(@Query("q") String q, @Query("appid") String appid);

    @POST("/api/users")
    Call<User1> createUser(@Body User1 user);

    @GET("/api/users?")
    Call<UserList> doGetUserList(@Query("page") String page);

    @FormUrlEncoded
    @POST("/api/users?")
    Call<UserList> doCreateUserWithField(@Field("name") String name, @Field("job") String job);
}