package com.inpetrosys.goho.fragment.post;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.inpetrosys.goho.Interfaceuser.CallBackInterface;
import com.inpetrosys.goho.Interfaceuser.OnClickerListener;
import com.inpetrosys.goho.Interfaceuser.Presenterinterface;
import com.inpetrosys.goho.R;
import com.inpetrosys.goho.activity.HomeActivity;
import com.inpetrosys.goho.adapter.PostAdapter;
import com.inpetrosys.goho.beans.Post;
import com.inpetrosys.goho.utility.UtilClass;

import java.util.ArrayList;


public class PostDialogFragment extends DialogFragment implements View.OnClickListener, Presenterinterface {
    private HomeActivity activity;
    private Context context;
    private UtilClass utilClass;
    private boolean isPressedButton;
    private ImageView img_close, img_post;
    private PostPresenter postPresenter;
    private RecyclerView rv_post_list;
    private ArrayList<Post> postArrayList=new ArrayList<>();
    private LinearLayoutManager layoutManager;
    private PostAdapter postAdapter;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = activity = (HomeActivity) getActivity();
        utilClass = UtilClass.getInstance();
        postPresenter = new PostPresenter(context, this);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        setStyle(DialogFragment.STYLE_NO_TITLE, getTheme());
        final Dialog dialog = super.onCreateDialog(savedInstanceState);
//        dialog.getWindow().getAttributes().windowAnimations = R.style.MyCustomTheme;
        return dialog;

    }

    @Override
    public Dialog getDialog() {
        return super.getDialog();
    }

    @Override
    public void onResume() {
        super.onResume();
        int width = getResources().getDisplayMetrics().widthPixels;
        int height = getResources().getDisplayMetrics().heightPixels;
        getDialog().getWindow().setLayout(width, (height - 100));
//        new LoadDataAsynktask().execute();
    }

    @Override
    public void onStart() {
        super.onStart();
        // safety check
        if (getDialog() == null) {
            return;
        }

        // set the animations to use on showing and hiding the dialog
        getDialog().getWindow().setWindowAnimations(
                R.style.dialog_slide_animation);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.post_screen, container, false);

        try {
            initView(view);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return view;
    }

    private void initView(final View view) {
        img_close = view.findViewById(R.id.img_close);
        img_close.setOnClickListener(this);
        img_post = view.findViewById(R.id.img_post);
        img_post.setOnClickListener(this);

        rv_post_list=view.findViewById(R.id.rv_post_list);
        layoutManager=new LinearLayoutManager(context);
        rv_post_list.setLayoutManager(layoutManager);
        setAdapter();

    }

    private void setAdapter() {
        postArrayList.add(new Post());
        postArrayList.add(new Post());
        postArrayList.add(new Post());
        postArrayList.add(new Post());
        postArrayList.add(new Post());
        postArrayList.add(new Post());
        postArrayList.add(new Post());
        postArrayList.add(new Post());
        postAdapter=new PostAdapter(context, postArrayList, new OnClickerListener() {
            @Override
            public void onClick(int pos) {

            }
        });

        rv_post_list.setAdapter(postAdapter);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (!isPressedButton) {
//            Blurry.delete(constaint_main_view);
            ImageView imageView = activity.findViewById(R.id.img_screenshots);
            imageView.setImageBitmap(null);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onClick(View view) {
        utilClass.perfomroneClick();
        switch (view.getId()) {
            case R.id.img_close:
                getDialog().dismiss();
                break;
            case R.id.img_post:
                postPresenter.callPost();
                break;
        }
    }

    @Override
    public void success() {

    }

    @Override
    public void fail(String msg) {

    }
    CallBackInterface callBackInterface;

    public void setCallBack(CallBackInterface callBackInterface) {
        this.callBackInterface = callBackInterface;
    }
}
