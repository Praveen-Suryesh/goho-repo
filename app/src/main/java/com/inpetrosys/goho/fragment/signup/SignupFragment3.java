package com.inpetrosys.goho.fragment.signup;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.hbb20.CountryCodePicker;
import com.inpetrosys.goho.Interfaceuser.Presenterinterface;
import com.inpetrosys.goho.R;
import com.inpetrosys.goho.activity.HomeActivity;
import com.inpetrosys.goho.activity.StartActivity;
import com.inpetrosys.goho.beans.User;
import com.inpetrosys.goho.session.SharePrefencesUtil;

public class SignupFragment3 extends Fragment implements Presenterinterface {
    private StartActivity activity;
    private Context context;
    private EditText txt_phone_number;
    private ImageView img_nxt;
    private SignupPresenter3 signupPresenter3;
    private SharePrefencesUtil sharePrefencesUtil;
    private TextView tv_flag;
    private CountryCodePicker ccp;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = activity = (StartActivity) getActivity();
        sharePrefencesUtil=new SharePrefencesUtil(context);
        signupPresenter3=new SignupPresenter3(context,this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.signup_screen3, container, false);
       try {
           initView(view);
       }catch (Exception e ){
           e.printStackTrace();
       }
        return view;
    }

    private void initView(View view) {
        ccp=view.findViewById(R.id.ccp);
        ccp.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
            @Override
            public void onCountrySelected() {
//                Toast.makeText(getContext(), "Updated " + ccp.getSelectedCountryCode()
//                      , Toast.LENGTH_SHORT).show();
            }
        });
        txt_phone_number = view.findViewById(R.id.txt_phone_number);
        img_nxt = view.findViewById(R.id.img_nxt);
        tv_flag = view.findViewById(R.id.tv_flag);
        tv_flag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        img_nxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signupPresenter3.validateParameter(txt_phone_number.getText().toString());

            }
        });
    }

    @Override
    public void success() {
        User user = null;
        if(sharePrefencesUtil.getUser()!=null) {
             user = sharePrefencesUtil.getUser();
        }else{
            user=new User();
        }
        user.setMobileNumber(txt_phone_number.getText().toString());
        sharePrefencesUtil.saveUser(user);
        Intent intent = new Intent(context, HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        activity.finish();
    }

    @Override
    public void fail(String msg) {
        Snackbar snackbar = Snackbar
                .make(img_nxt.getRootView(), msg, Snackbar.LENGTH_LONG);
        snackbar.setActionTextColor(Color.RED);
        View snackbarView = snackbar.getView();
        snackbarView.setBackgroundColor(context.getResources().getColor(R.color.black));
        TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        textView.setTextSize(18);
        snackbar.show();
    }
}
