package com.inpetrosys.goho.fragment.signup;

import android.content.Context;

import com.inpetrosys.goho.Interfaceuser.Presenterinterface;
import com.inpetrosys.goho.R;
import com.inpetrosys.goho.utility.UtilClass;

public class SignupPresenter {
    private Presenterinterface signupinterface;
    private Context context;

    SignupPresenter(Context context,Presenterinterface signupinterface) {
        this.signupinterface = signupinterface;
        this.context = context;
    }
    public void validateParameter(String date, String month, String year){
        if(signupinterface!=null){
            if(date.equalsIgnoreCase("DD")){
                signupinterface.fail(context.getString(R.string.datevalidate));
                return;
            }else if(month.equalsIgnoreCase("MM")){
                signupinterface.fail(context.getString(R.string.monthvalidate));
                return;
            }else if(year.equalsIgnoreCase("YYYY")){
                signupinterface.fail(context.getString(R.string.yearvalidate));
                return;
            }else if(!UtilClass.isThisDateValid(date+"/"+month+"/"+year,"dd/MM/yyyy")){
                signupinterface.fail(context.getString(R.string.invaliddatevalidate));
                return;
            }else{
                signupinterface.success();
                return;
            }
        }
    }
}
