package com.inpetrosys.goho.fragment.signup;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.inpetrosys.goho.R;
import com.inpetrosys.goho.activity.StartActivity;
import com.inpetrosys.goho.fragment.VerifyOTPFragment;
public class SignupFragment4 extends Fragment {
    private StartActivity activity;
    private Context context;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context=activity=(StartActivity) getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.signup_screen4,container,false);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                activity.replaceFragment(new VerifyOTPFragment());
            }
        },1000);
        return view;
    }
}
