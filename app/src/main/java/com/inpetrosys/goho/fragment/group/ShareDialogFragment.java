//package com.inpetrosys.goho.fragment.group;
//
//import android.app.Dialog;
//import android.content.Context;
//import android.os.Bundle;
//import android.support.annotation.NonNull;
//import android.support.annotation.Nullable;
//import android.support.v4.app.DialogFragment;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.Button;
//import android.widget.CheckBox;
//import android.widget.TextView;
//
//import com.inpetrosys.goho.R;
//import com.inpetrosys.goho.activity.HomeActivity;
//
//public class ShareDialogFragment extends DialogFragment implements View.OnClickListener {
//    private HomeActivity activity;
//    private Context context;
//    private Button btn_saveandshare;
//    private TextView tv_time, tv_plus, tv_min;
//    private CheckBox chk_visible, chk_invisible;
//    private int defaltValue = 0;
//
//    private boolean isVisible = true;
//
//    @Override
//    public void onCreate(@Nullable Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        context = activity = (HomeActivity) getActivity();
//    }
//
//    @NonNull
//    @Override
//    public Dialog onCreateDialog(Bundle savedInstanceState) {
//        setStyle(DialogFragment.STYLE_NO_TITLE, getTheme());
//        return super.onCreateDialog(savedInstanceState);
//
//    }
//
//    @Override
//    public Dialog getDialog() {
//        return super.getDialog();
//    }
//
//    @Override
//    public void onResume() {
//        super.onResume();
//        int width = getResources().getDisplayMetrics().widthPixels;
//        int height = getResources().getDisplayMetrics().heightPixels;
//        getDialog().getWindow().setLayout(width, height);
////        new LoadDataAsynktask().execute();
//    }
//
//    @Override
//    public void onStart() {
//        super.onStart();
//
//    }
//
//    @Nullable
//    @Override
//    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        View view = inflater.inflate(R.layout.sharelocation_dialog, container, false);
//        try {
//            initView(view);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return view;
//    }
//
//    private void initView(final View view) {
//
//        btn_saveandshare = view.findViewById(R.id.btn_saveandshare);
//        btn_saveandshare.setOnClickListener(this);
//
//        tv_plus = view.findViewById(R.id.tv_plus);
//        tv_plus.setOnClickListener(this);
//        tv_min = view.findViewById(R.id.tv_min);
//        tv_min.setOnClickListener(this);
//        chk_visible = view.findViewById(R.id.chk_visible);
//        chk_visible.setOnClickListener(this);
//        chk_invisible = view.findViewById(R.id.chk_invisible);
//        chk_invisible.setOnClickListener(this);
//    }
//
//
//    @Override
//    public void onClick(View v) {
//        switch (v.getId()) {
//            case R.id.btn_saveandshare:
//                getDialog().dismiss();
//                activity.getIntent().putExtra("visible", isVisible);
//                activity.getIntent().putExtra("defaltValue", defaltValue);
//
//
//                break;
//
//            case R.id.tv_plus:
//                ++defaltValue;
//                String str=defaltValue+"";
//                tv_time.setText(str);
//                break;
//            case R.id.tv_min:
//                if (defaltValue > 0) {
//                    --defaltValue;
//                }
//                String str2=defaltValue+"";
//                tv_time.setText(str2);
//                break;
//            case R.id.chk_visible:
//                if (!isVisible) {
//                    chk_visible.setChecked(true);
//                    chk_invisible.setChecked(false);
//                    isVisible = true;
//                }
//                break;
//            case R.id.chk_invisible:
//                if (!isVisible) {
//                    chk_visible.setChecked(false);
//                    chk_invisible.setChecked(true);
//                    isVisible = false;
//                }
//                break;
//        }
//    }
//}