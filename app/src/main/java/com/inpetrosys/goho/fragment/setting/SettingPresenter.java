package com.inpetrosys.goho.fragment.setting;

import android.content.Context;
import android.text.TextUtils;

import com.inpetrosys.goho.Interfaceuser.Presenterinterface;
import com.inpetrosys.goho.R;

public class SettingPresenter {
    private Presenterinterface presenterinterface;
    private Context context;

    SettingPresenter(Context context, Presenterinterface presenterinterface) {
        this.presenterinterface = presenterinterface;
        this.context = context;
    }

    public void validateParameter(String userId,String friendId) {
        if (presenterinterface != null) {
            if (TextUtils.isEmpty(userId)) {
                presenterinterface.fail(context.getString(R.string.enteruserid));
                return;
            }
            else if (userId.length()!=10) {
                presenterinterface.fail(context.getString(R.string.useridlength));
                return;
            }
            else if (TextUtils.isEmpty(friendId)) {
                presenterinterface.fail(context.getString(R.string.enterfriendid));
                return;
            } else if (friendId.length() != 10) {
                presenterinterface.fail(context.getString(R.string.friendidlength));
                return;
            }
            else {
                presenterinterface.success();
                return;
            }
        }
    }

    public boolean isEmpty(String copyTxt) {
        if (TextUtils.isEmpty(copyTxt))
            return true;
        else
            return false;
    }

}
