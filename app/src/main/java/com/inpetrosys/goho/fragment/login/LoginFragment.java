package com.inpetrosys.goho.fragment.login;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.inpetrosys.goho.Interfaceuser.Presenterinterface;
import com.inpetrosys.goho.R;
import com.inpetrosys.goho.activity.StartActivity;
import com.inpetrosys.goho.beans.User;
import com.inpetrosys.goho.fragment.ResendOTPFragment;
import com.inpetrosys.goho.fragment.signup.SignupFragment3;
import com.inpetrosys.goho.session.SharePrefencesUtil;
import com.inpetrosys.goho.utility.UtilClass;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;


public class LoginFragment extends Fragment implements View.OnClickListener, Presenterinterface {
    private StartActivity activity;
    private Context context;
    private LoginButton loginButton;
    private EditText et_mobnumber;
    private Button txt_goho,txt_face1;
    private TextView txt_line, txt_wechat, txt_readplicy;
    private CheckBox img_chk;
    private SharePrefencesUtil sharePrefencesUtil;
    private boolean isChk;
    private ConstraintLayout constraintLay;
    private LoginPresenter loginPresenter;

    private CallbackManager callbackManager;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = activity = (StartActivity) getActivity();
        loginPresenter = new LoginPresenter(context, this);
        sharePrefencesUtil = new SharePrefencesUtil(context);


    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.login_screen, container, false);
        try {
            initView(view);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return view;
    }

    private void initView(View view) {
        constraintLay = view.findViewById(R.id.constraintLay);
        et_mobnumber = view.findViewById(R.id.et_mobnumber);
        txt_goho = view.findViewById(R.id.txt_goho);
        txt_face1 = view.findViewById(R.id.txt_face1);
        txt_face1.setOnClickListener(this);
        txt_line = view.findViewById(R.id.txt_line);
        txt_wechat = view.findViewById(R.id.txt_wechat);
        txt_readplicy = view.findViewById(R.id.txt_readplicy);
        img_chk = view.findViewById(R.id.img_chk);
        img_chk.setOnClickListener(this);
        isChk = sharePrefencesUtil.getBoolean("remember");
        if (isChk) {
            img_chk.setChecked(true);
        } else {
            img_chk.setChecked(false);
        }
        txt_goho.setOnClickListener(this);

        callbackManager = CallbackManager.Factory.create();
        loginButton = (LoginButton) view.findViewById(R.id.login_button);
        loginButton.setReadPermissions(Arrays.asList("email", "public_profile", "user_birthday"));
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                try {
                    getUserDetails(loginResult);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onCancel() {
                // App code
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        // Logs 'install' and 'app activate' App Events.
    }

    @Override
    public void onPause() {
        super.onPause();
        // Logs 'app deactivate' App Event.
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    protected void getUserDetails(LoginResult loginResult) {
        GraphRequest data_request = GraphRequest.newMeRequest(
                loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(
                            JSONObject json_object,
                            GraphResponse response) {

                        Log.i("LoginActivity",
                                response.toString());
                        User user = new User();
                        try {
                            if (json_object.has("id")) {
                                String id = json_object.getString("id");
                                try {
                                    URL profile_pic = new URL(
                                            "http://graph.facebook.com/" + id + "/picture?type=large");
                                    Log.i("profile_pic",
                                            profile_pic + "");

                                    user.setPic(profile_pic.toString());

                                } catch (MalformedURLException e) {
                                    e.printStackTrace();
                                }
                            }
                            if (json_object.has("email")) {
                                String email = json_object.getString("email");
                            }
//                            String gender = json_object.getString("gender");
                            if (json_object.has("name"))
                                user.setName(json_object.getString("name"));
                            if (json_object.has("birthday"))
                                user.setBirthDay(json_object.getString("birthday"));
                            sharePrefencesUtil.saveUser(user);
                            LoginManager.getInstance().logOut();
                            activity.replaceFragment(new SignupFragment3());

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                });
        Bundle permission_param = new Bundle();
        permission_param.putString("fields", "id,name,email,birthday,picture.width(120).height(120)");
        data_request.setParameters(permission_param);
        data_request.executeAsync();

    }

    // variable to track event time
    private long mLastClickTime = 0;

    @Override
    public void onClick(View v) {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
            return;
        }
        mLastClickTime = SystemClock.elapsedRealtime();
        switch (v.getId()) {
            case R.id.txt_goho:
                loginPresenter.validateParameter(et_mobnumber.getText().toString());
                break;
            case R.id.img_chk:
                if (isChk) {
                    img_chk.setChecked(false);
                    sharePrefencesUtil.setBoolean(false, "remember");
                    isChk = false;
                } else {
                    img_chk.setChecked(true);
                    sharePrefencesUtil.setBoolean(true, "remember");
                    isChk = true;
                }
                break;
            case R.id.txt_face1:
//                loginButton.performClick();
                facebookLogin();
                break;
        }
    }
    public void facebookLogin(){
        callbackManager = CallbackManager.Factory.create();

        LoginManager.getInstance().logInWithReadPermissions(activity, Arrays.asList("public_profile"));

        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject json_object, GraphResponse response) {
                                Log.i("LoginActivity",
                                        response.toString());
                                User user = new User();
                                try {
                                    if (json_object.has("id")) {
                                        String id = json_object.getString("id");
                                        try {
                                            URL profile_pic = new URL(
                                                    "http://graph.facebook.com/" + id + "/picture?type=large");
                                            Log.i("profile_pic",
                                                    profile_pic + "");

                                            user.setPic(profile_pic.toString());

                                        } catch (MalformedURLException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                    if (json_object.has("email")) {
                                        String email = json_object.getString("email");
                                    }
//                            String gender = json_object.getString("gender");
                                    if (json_object.has("name"))
                                        user.setName(json_object.getString("name"));
                                    if (json_object.has("birthday"))
                                        user.setBirthDay(json_object.getString("birthday"));
                                    sharePrefencesUtil.saveUser(user);
                                    LoginManager.getInstance().logOut();
                                    activity.replaceFragment(new SignupFragment3());

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,birthday,picture.width(120).height(120)");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                Log.i("MainActivity", "@@@onCancel");
            }

            @Override
            public void onError(FacebookException error) {
                Log.i("MainActivity", "@@@onError: "+ error.getMessage());
            }
        });
    }
    @Override
    public void success() {
        UtilClass.hideSoftKeyboard(activity);
        activity.replaceFragment(new ResendOTPFragment());
    }

    @Override
    public void fail(String msg) {
        UtilClass.hideSoftKeyboard(activity);
        Snackbar snackbar = Snackbar
                .make(constraintLay, msg, Snackbar.LENGTH_LONG);
        snackbar.setActionTextColor(Color.RED);
        View snackbarView = snackbar.getView();
        snackbarView.setBackgroundColor(context.getResources().getColor(R.color.black));
        TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        textView.setTextSize(18);
        snackbar.show();
    }
}
