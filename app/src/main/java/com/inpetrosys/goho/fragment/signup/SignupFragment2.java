package com.inpetrosys.goho.fragment.signup;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.inpetrosys.goho.Interfaceuser.Presenterinterface;
import com.inpetrosys.goho.R;
import com.inpetrosys.goho.activity.StartActivity;
import com.inpetrosys.goho.utility.UtilClass;

import java.util.Arrays;
import java.util.List;

public class SignupFragment2 extends Fragment implements Presenterinterface {
    private StartActivity activity;
    private Context context;
    private ImageButton img_nxt;
    private Spinner spinner_dd, spinner_mm, spinner_yyyy;
    private SignupPresenter signupPresenter;
    private String year = "", date = "", month = "";
    private ConstraintLayout coordinatorLayout;
    private TextView txt_signupque2;
    private String usrName;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = activity = (StartActivity) getActivity();
        signupPresenter = new SignupPresenter(context, this);
        if (getArguments() != null && getArguments().containsKey("name")) {
            usrName = getArguments().getString("name");
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.signup_screen2, container, false);
     try {
         initView(view);
     }catch (Exception e){
         e.printStackTrace();
     }
        return view;
    }

    private void initView(View view) {
        coordinatorLayout = view.findViewById(R.id.coordinatorLayout);
        img_nxt = view.findViewById(R.id.img_nxt);
        spinner_dd = view.findViewById(R.id.spinner_dd);
        spinner_mm = view.findViewById(R.id.spinner_mm);
        spinner_yyyy = view.findViewById(R.id.spinner_yyyy);

        txt_signupque2 = view.findViewById(R.id.txt_signupque2);
        txt_signupque2.setText("Hey " + usrName + ", when's your birthday?");

        String[] dd_array = context.getResources().getStringArray(R.array.date);
        String[] mm_array = context.getResources().getStringArray(R.array.month);
        List<String> dateArrayList = Arrays.asList(dd_array);
        List<String> monthArrayList = Arrays.asList(mm_array);


        // Creating adapter for spinner Date
        ArrayAdapter<String> ddAdapter = new ArrayAdapter<String>(context, R.layout.spinner_defalt_view, dateArrayList);

        // Drop down layout style - list view with radio button
        ddAdapter.setDropDownViewResource(R.layout.spinner_item_view);
        // attaching data adapter to spinner
        spinner_dd.setAdapter(ddAdapter);

        spinner_dd.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                date = parent.getItemAtPosition(position).toString();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        // Creating adapter for spinner Month
        ArrayAdapter<String> mmAdapter = new ArrayAdapter<String>(context, R.layout.spinner_defalt_view, monthArrayList);

        // Drop down layout style - list view with radio button
        mmAdapter.setDropDownViewResource(R.layout.spinner_item_view);
        // attaching data adapter to spinner
        spinner_mm.setAdapter(mmAdapter);

        spinner_mm.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                month = parent.getItemAtPosition(position).toString();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        // Creating adapter for spinner Date
        ArrayAdapter<String> yyAdapter = new ArrayAdapter<String>(context, R.layout.spinner_defalt_view, UtilClass.getYearList());

        // Drop down layout style - list view with radio button
        yyAdapter.setDropDownViewResource(R.layout.spinner_item_view);
        // attaching data adapter to spinner
        spinner_yyyy.setAdapter(yyAdapter);

        spinner_yyyy.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                year = parent.getItemAtPosition(position).toString();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        img_nxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signupPresenter.validateParameter(date, month, year);
            }
        });

    }


    @Override
    public void success() {
        activity.replaceFragment(new SignupFragment3());
    }

    @Override
    public void fail(String msg) {
        Snackbar snackbar = Snackbar
                .make(coordinatorLayout, msg, Snackbar.LENGTH_LONG);
        snackbar.setActionTextColor(Color.RED);
        View snackbarView = snackbar.getView();
        snackbarView.setBackgroundColor(context.getResources().getColor(R.color.black));
        TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        textView.setTextSize(18);
        snackbar.show();
    }
}
