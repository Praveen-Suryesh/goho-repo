package com.inpetrosys.goho.fragment.login;

import android.content.Context;
import android.text.TextUtils;

import com.inpetrosys.goho.Interfaceuser.Presenterinterface;
import com.inpetrosys.goho.R;
import com.inpetrosys.goho.utility.UtilClass;

public class LoginPresenter {
    private Presenterinterface presenterinterface;
    private Context context;

    LoginPresenter(Context context, Presenterinterface presenterinterface) {
        this.presenterinterface = presenterinterface;
        this.context = context;
    }

    public void validateParameter(String mobileNumber) {
        if (presenterinterface != null) {
            if (TextUtils.isEmpty(mobileNumber)) {
                presenterinterface.fail(context.getString(R.string.mobnumvalide));
                return;
            } else if (!UtilClass.isValidMobile(mobileNumber)) {
                presenterinterface.fail(context.getString(R.string.validmobnum));
                return;
            } else {
                presenterinterface.success();
                return;
            }
        }
    }
}
