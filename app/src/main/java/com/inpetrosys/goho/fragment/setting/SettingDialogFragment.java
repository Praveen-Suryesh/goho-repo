package com.inpetrosys.goho.fragment.setting;

import android.app.Activity;
import android.app.Dialog;
import android.content.ClipData;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.inpetrosys.goho.Interfaceuser.CallBackInterface;
import com.inpetrosys.goho.Interfaceuser.OnClickerListener;
import com.inpetrosys.goho.Interfaceuser.Presenterinterface;
import com.inpetrosys.goho.R;
import com.inpetrosys.goho.activity.HomeActivity;
import com.inpetrosys.goho.adapter.ContactAdapter;
import com.inpetrosys.goho.adapter.GroupAdapter;
import com.inpetrosys.goho.beans.Friend;
import com.inpetrosys.goho.customview.CustomScrollView;
import com.inpetrosys.goho.fragment.group.GroupPresenter;
import com.inpetrosys.goho.utility.ImageConverter;
import com.inpetrosys.goho.utility.UtilClass;

import org.w3c.dom.Text;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Set;


public class SettingDialogFragment extends DialogFragment implements View.OnClickListener
        , Presenterinterface {
    private HomeActivity activity;
    private Context context;
    private ImageView img_close, img_user_profile;
    private TextView txt_changephoto, txt_status, txt_acountid, txt_no_blocked, txt_backup,
            txt_num_postvisible;
    private Switch swict_location, swicth_photo, swicth_notification;
    private UtilClass utilClass;
    private SettingPresenter settingPresenter;
    private boolean isPressedButton;
    private String userChoosenTask;

    private int GALLERY = 1, CAMERA = 2;
    private static final String IMAGE_DIRECTORY = "/goho_image";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = activity = (HomeActivity) getActivity();
        utilClass = UtilClass.getInstance();
        settingPresenter = new SettingPresenter(context, this);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        setStyle(DialogFragment.STYLE_NO_TITLE, getTheme());
        final Dialog dialog = super.onCreateDialog(savedInstanceState);
        return dialog;

    }

    @Override
    public Dialog getDialog() {
        return super.getDialog();
    }

    @Override
    public void onResume() {
        super.onResume();
        int width = getResources().getDisplayMetrics().widthPixels;
        int height = getResources().getDisplayMetrics().heightPixels;
        getDialog().getWindow().setLayout(width, (height - 100));
    }

    @Override
    public void onStart() {
        super.onStart();
        // safety check
        if (getDialog() == null) {
            return;
        }

        // set the animations to use on showing and hiding the dialog
        getDialog().getWindow().setWindowAnimations(
                R.style.dialog_slide_animation);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.setting_screen, container, false);

        try {
            initView(view);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return view;
    }

    private void initView(final View view) {
        img_close = view.findViewById(R.id.img_close);
        img_close.setOnClickListener(this);
        img_user_profile = view.findViewById(R.id.img_user_profile);
        txt_changephoto = view.findViewById(R.id.txt_changephoto);
        txt_changephoto.setOnClickListener(this);
        txt_status = view.findViewById(R.id.txt_status);
        txt_acountid = view.findViewById(R.id.txt_acountid);
        txt_no_blocked = view.findViewById(R.id.txt_no_blocked);
        txt_backup = view.findViewById(R.id.txt_backup);
        txt_num_postvisible = view.findViewById(R.id.txt_num_postvisible);
        swict_location = (Switch) view.findViewById(R.id.switch_location);
        swicth_photo = view.findViewById(R.id.switch_photo);
        swicth_notification = view.findViewById(R.id.switch_notification);

        // Set a checked change listener for switch button
        swict_location.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    swict_location.setThumbResource(R.drawable.backgraund);
                    swict_location.setTrackResource(R.drawable.customtrack);
                } else {
                    swict_location.setThumbResource(R.drawable.backgraund_gray);
                    swict_location.setTrackResource(R.drawable.customtrack_gray);
                }
            }
        });
        swicth_photo.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    swicth_photo.setThumbResource(R.drawable.backgraund);
                    swicth_photo.setTrackResource(R.drawable.customtrack);
                } else {
                    swicth_photo.setThumbResource(R.drawable.backgraund_gray);
                    swicth_photo.setTrackResource(R.drawable.customtrack_gray);
                }
            }
        });
        swicth_notification.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    swicth_notification.setThumbResource(R.drawable.backgraund);
                    swicth_notification.setTrackResource(R.drawable.customtrack);
                } else {
                    swicth_notification.setThumbResource(R.drawable.backgraund_gray);
                    swicth_notification.setTrackResource(R.drawable.customtrack_gray);
                }
            }
        });
    }

    public void togglestatehandler() {
//        Switch switchbtn = (Switch) v;
        boolean isChecked = swict_location.isChecked();
        if (isChecked) {
            Toast.makeText(context, "On......", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(context, "Off......", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (!isPressedButton) {
//            Blurry.delete(constaint_main_view);
            ImageView imageView = activity.findViewById(R.id.img_screenshots);
            imageView.setImageBitmap(null);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void addPhoto() {
        final Dialog dialog = new Dialog(context, R.style.dialog_slide_animation);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // Include dialog.xml file
        dialog.setContentView(R.layout.take_photo_dialog);
        Button btn_take_photo = dialog.findViewById(R.id.btn_take_photo);
        Button btn_gallery = dialog.findViewById(R.id.btn_gallery);
        Button btn_cancel = dialog.findViewById(R.id.btn_cancel);
        btn_take_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                utilClass.perfomroneClick();
                dialog.dismiss();
                userChoosenTask = "Take Photo";
                takePhotoFromCamera();
            }
        });

        btn_gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                utilClass.perfomroneClick();
                dialog.dismiss();
                userChoosenTask = "Choose from Library";
                choosePhotoFromGallary();
            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                utilClass.perfomroneClick();
                dialog.dismiss();
            }
        });
        int width = getResources().getDisplayMetrics().widthPixels;
        dialog.getWindow().setLayout(width - 100, ConstraintLayout.LayoutParams.WRAP_CONTENT);

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {
//                revealShow(dialogView, true, null);
            }
        });

        dialog.setOnKeyListener(new Dialog.OnKeyListener() {

            @Override
            public boolean onKey(DialogInterface arg0, int keyCode,
                                 KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    dialog.dismiss();
                }
                return true;
            }
        });
//        dialog.getWindow().getDecorView().setBackgroundResource(android.R.color.black);
//        dialog.getWindow().setDimAmount(0.5f);
        dialog.show();
    }

    private void takePhotoFromCamera() {
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, CAMERA);
    }

    public void choosePhotoFromGallary() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(galleryIntent, GALLERY);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == activity.RESULT_CANCELED) {
            return;
        }
        if (userChoosenTask.equalsIgnoreCase("Choose from Library")) {
            if (data != null) {
                Uri contentURI = data.getData();
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), contentURI);
                    Bitmap circularBitmap = ImageConverter.getRoundedCornerBitmap(bitmap, 150);
                    Toast.makeText(context, "Image Saved!", Toast.LENGTH_SHORT).show();
                    Drawable drawable = new BitmapDrawable(getResources(), circularBitmap);
                    img_user_profile.setImageDrawable(drawable);

                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(context, "Failed!", Toast.LENGTH_SHORT).show();
                }
            }

        } else if (userChoosenTask.equalsIgnoreCase("Take Photo")) {
            Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
            img_user_profile.setImageBitmap(thumbnail);
            saveImage(thumbnail);
            Toast.makeText(context, "Image Saved!", Toast.LENGTH_SHORT).show();
        }
    }

    public String saveImage(Bitmap myBitmap) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        File wallpaperDirectory = new File(
                Environment.getExternalStorageDirectory() + IMAGE_DIRECTORY);
        // have the object build the directory structure, if needed.
        if (!wallpaperDirectory.exists()) {
            wallpaperDirectory.mkdirs();
        }

        try {
            File f = new File(wallpaperDirectory, Calendar.getInstance()
                    .getTimeInMillis() + ".jpg");
            f.createNewFile();
            FileOutputStream fo = new FileOutputStream(f);
            fo.write(bytes.toByteArray());
            MediaScannerConnection.scanFile(context,
                    new String[]{f.getPath()},
                    new String[]{"image/jpeg"}, null);
            fo.close();
            Log.d("TAG", "File Saved::--->" + f.getAbsolutePath());

            return f.getAbsolutePath();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return "";
    }

    @Override
    public void onClick(View view) {
        utilClass.perfomroneClick();
        switch (view.getId()) {
            case R.id.img_close:
                getDialog().dismiss();
                break;
            case R.id.txt_changephoto:
                addPhoto();
                break;
        }
    }

    CallBackInterface callBackInterface;

    public void setCallBack(CallBackInterface callBackInterface) {
        this.callBackInterface = callBackInterface;
    }

    @Override
    public void success() {

    }

    @Override
    public void fail(String msg) {

    }

}
