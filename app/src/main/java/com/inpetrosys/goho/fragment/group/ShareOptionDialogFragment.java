//package com.inpetrosys.goho.fragment.group;
//
//import android.app.Dialog;
//import android.content.Context;
//import android.os.Bundle;
//import android.support.annotation.NonNull;
//import android.support.annotation.Nullable;
//import android.support.constraint.ConstraintLayout;
//import android.support.v4.app.DialogFragment;
//import android.support.v4.app.FragmentManager;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.Button;
//
//import com.inpetrosys.goho.R;
//import com.inpetrosys.goho.activity.HomeActivity;
//import com.inpetrosys.goho.utility.UtilClass;
//
//public class ShareOptionDialogFragment extends DialogFragment {
//    private HomeActivity activity;
//    private Context context;
//    private Button btn_yes, btn_no;
//    private ConstraintLayout constraintLay_share;
//
//    @Override
//    public void onCreate(@Nullable Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        context = activity = (HomeActivity) getActivity();
//    }
//
//    @NonNull
//    @Override
//    public Dialog onCreateDialog(Bundle savedInstanceState) {
//        setStyle(DialogFragment.STYLE_NO_TITLE, getTheme());
//        return super.onCreateDialog(savedInstanceState);
//
//    }
//
//    @Override
//    public Dialog getDialog() {
//        return super.getDialog();
//    }
//
//    @Override
//    public void onResume() {
//        super.onResume();
//        int width = getResources().getDisplayMetrics().widthPixels;
//        int height = getResources().getDisplayMetrics().heightPixels;
////        ConstraintLayout.LayoutParams params= (ConstraintLayout.LayoutParams) constraintLay_share.getLayoutParams();
////        params.height=ConstraintLayout.LayoutParams.WRAP_CONTENT;
//        getDialog().getWindow().setLayout(width-100,ConstraintLayout.LayoutParams.WRAP_CONTENT);
////        new LoadDataAsynktask().execute();
//    }
//
//    @Override
//    public void onStart() {
//        super.onStart();
//
//    }
//
//    @Nullable
//    @Override
//    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        View view = inflater.inflate(R.layout.sharelocation_option_dialog, container, false);
//        try {
//            initView(view);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return view;
//    }
//
//    private void initView(final View view) {
//        constraintLay_share=view.findViewById(R.id.constraintLay_share);
//        btn_yes = view.findViewById(R.id.btn_yes);
//        btn_yes.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                getDialog().dismiss();
//                UtilClass.hideSoftKeyboard(activity);
//                FragmentManager fm1 = getFragmentManager();
//                UtilClass.hideSoftKeyboard(activity);
//                ShareDialogFragment shareDialogFragment = new ShareDialogFragment();
//                shareDialogFragment.show(fm1, "dialog");
////                getDialog().dismiss();
//            }
//        });
//        btn_no = view.findViewById(R.id.btn_no);
//        btn_no.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                getDialog().dismiss();
//            }
//        });
//    }
//}