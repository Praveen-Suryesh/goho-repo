package com.inpetrosys.goho.fragment;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.inpetrosys.goho.R;
import com.inpetrosys.goho.activity.StartActivity;
import com.inpetrosys.goho.fragment.login.LoginFragment;
import com.inpetrosys.goho.fragment.signup.SignupFragment1;
import com.inpetrosys.goho.utility.CustomTypefaceSpan;

public class ResendOTPFragment extends Fragment implements View.OnClickListener {
    private StartActivity activity;
    private Context context;
    private TextView txt_code, txt_wait, txt_resend, txt_change_Mob;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = activity = (StartActivity) getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.otp_again_screen, container, false);
        try {
            initView(view);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return view;
    }

    private void initView(View view) {
        txt_code = view.findViewById(R.id.txt_code);
        txt_wait = view.findViewById(R.id.txt_wait);
//        txt_wait.setEnabled(false);
        txt_wait.setOnClickListener(this);
        txt_resend = view.findViewById(R.id.txt_resend);
        txt_change_Mob = view.findViewById(R.id.txt_change_Mob);
        txt_change_Mob.setOnClickListener(this);
        Typeface font2 = Typeface.createFromAsset(context.getAssets(), "TitilliumWeb-Bold.ttf");
        SpannableStringBuilder SS = new SpannableStringBuilder(context.getResources().getString(R.string.resendmsg));
        SS.setSpan(new CustomTypefaceSpan("", font2), 25, 36, Spanned.SPAN_EXCLUSIVE_INCLUSIVE);

        txt_resend.setText(SS);

        //on Condiation call
//        txt_wait.setText(context.getResources().getString(R.string.verify));
//        txt_wait.setBackgroundResource(R.drawable.black_round_view);
//        txt_resend.setVisibility(View.INVISIBLE);
//        txt_change_Mob.setVisibility(View.INVISIBLE);
//        txt_wait.setEnabled(true);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_change_Mob:
                activity.replaceFragment(new LoginFragment());
//                activity.backstackImmidiate("ResendOTPFragment");
                break;
            case R.id.txt_wait:
//                if(isRegistered){
//                Intent intent = new Intent(context, HomeActivity.class);
//                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                startActivity(intent);
//                activity.finish();
//        }else{
                activity.replaceFragment(new SignupFragment1());
//        }
                break;
        }
    }
}
