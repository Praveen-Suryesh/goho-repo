package com.inpetrosys.goho.fragment.group;

import android.app.Dialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.inpetrosys.goho.Interfaceuser.CallBackInterface;
import com.inpetrosys.goho.Interfaceuser.OnClickerListener;
import com.inpetrosys.goho.Interfaceuser.Presenterinterface;
import com.inpetrosys.goho.R;
import com.inpetrosys.goho.activity.HomeActivity;
import com.inpetrosys.goho.adapter.ContactAdapter;
import com.inpetrosys.goho.adapter.GroupAdapter;
import com.inpetrosys.goho.beans.Friend;
import com.inpetrosys.goho.beans.Group;
import com.inpetrosys.goho.customview.CustomScrollView;
import com.inpetrosys.goho.utility.UtilClass;

import java.util.ArrayList;

import jp.wasabeef.blurry.Blurry;

public class GroupGohoFragment extends DialogFragment implements View.OnClickListener
        , Presenterinterface {
    private HomeActivity activity;
    private Context context;
    private TextView txt_addgroup, tv_viewall, tv_selctall;
    private RecyclerView rv_groupList, rv_contactList;
    private CheckBox chk_contact_slect_all;

    private ArrayList<Group> groupArrayList = new ArrayList<>();
    private ArrayList<Friend> contactArrayList = new ArrayList<>();
    private RecyclerView.LayoutManager layoutManager, layoutManager2;
    private GroupAdapter groupAdapter;
    private ContactAdapter contactAdapter;
    //    private ConstraintLayout dialog;
    private CustomScrollView scrollViewNested;
    private boolean isViewAll;
    private boolean isChkAll;
    private ConstraintLayout constraintLay_container;
    private GroupPresenter groupPresenter;
    private boolean isClickButton;
    private boolean isPressedButton;
    private int intialTime = 500;
    private boolean isOnGroup = true;
    private UtilClass utilClass;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = activity = (HomeActivity) getActivity();
        groupArrayList = getArguments().getParcelableArrayList("group");
        contactArrayList = getArguments().getParcelableArrayList("contact");
        groupPresenter = new GroupPresenter(context, this);
        utilClass=UtilClass.getInstance();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        setStyle(DialogFragment.STYLE_NO_TITLE, getTheme());
        final Dialog dialog = super.onCreateDialog(savedInstanceState);
//        dialog.getWindow().getAttributes().windowAnimations = R.style.MyCustomTheme;
        return dialog;

    }

    @Override
    public Dialog getDialog() {
        return super.getDialog();
    }

    @Override
    public void onResume() {
        super.onResume();
        int width = getResources().getDisplayMetrics().widthPixels;
        int height = getResources().getDisplayMetrics().heightPixels;
        getDialog().getWindow().setLayout(width, (height - 100));
//        new LoadDataAsynktask().execute();
    }

    @Override
    public void onStart() {
        super.onStart();
        // safety check
        if (getDialog() == null) {
            return;
        }

        // set the animations to use on showing and hiding the dialog
        getDialog().getWindow().setWindowAnimations(
                R.style.dialog_slide_animation);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.friend_group_screen_dialog, container, false);

        try {
            initView(view);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return view;
    }

    //    Group View
    private ImageView img_close;
    private FrameLayout content_frame_dialg;
    private TextView tv_group, tv_goho;
    private RelativeLayout rl_groups;

    //    Goho View
    private TextView tv_copy;
    private Button btn_goho;
    private TextView tv_userid;
    private EditText edt_friendid;
    private ConstraintLayout constaint_main_view;

    private void initView(final View view) {
        constaint_main_view = activity.findViewById(R.id.constaint_main_view);
        img_close = view.findViewById(R.id.img_close);
        constraintLay_container = view.findViewById(R.id.constraintLay_container);
        content_frame_dialg = view.findViewById(R.id.content_frame_dialg);
        tv_group = view.findViewById(R.id.tv_group);
        rl_groups = view.findViewById(R.id.rl_groups);
        tv_goho = view.findViewById(R.id.tv_goho);


        rl_groups.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isOnGroup) {
                    setGroupView(view);
                    isOnGroup = true;
                }
            }
        });

        tv_goho.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isOnGroup) {
                    setGohoView(view);
                    isOnGroup = false;
                }
            }
        });

        img_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                utilClass.perfomroneClick();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
//                    revealShow(dialogView, true, dialog);
                            content_frame_dialg.removeAllViews();
                            getDialog().dismiss();
                        } else {
                            content_frame_dialg.removeAllViews();
                            getDialog().dismiss();
                        }
//                Blurry.delete(constaint_main_view);
//                ImageView imageView = activity.findViewById(R.id.img_screenshots);
//                imageView.setImageBitmap(null);
                        utilClass.getObjectAsntask();
                    }
                },500);

            }
        });
        setGroupView(view);
    }
    // variable to track event time
    private long mLastClickTime = 0;
    @Override
    public void onDestroy() {
        super.onDestroy();
        if (!isPressedButton) {
//            Blurry.delete(constaint_main_view);
            ImageView imageView = activity.findViewById(R.id.img_screenshots);
            imageView.setImageBitmap(null);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    private void setGohoView(View view) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View inflatedLayout = inflater.inflate(R.layout.goho_screen, null, false);
        if (content_frame_dialg.getChildCount() > 0)
            content_frame_dialg.removeAllViews();
        content_frame_dialg.addView(inflatedLayout);

        rl_groups.setBackgroundColor(getResources().getColor(R.color.light_gray));
        tv_goho.setTextColor(getResources().getColor(R.color.white));
        tv_group.setTextColor(getResources().getColor(R.color.black));
        tv_goho.setBackgroundColor(getResources().getColor(R.color.black));
        img_close.setImageResource(R.drawable.close_b);

        tv_copy = view.findViewById(R.id.tv_copy);
        tv_copy.setOnClickListener(this);
        btn_goho = view.findViewById(R.id.btn_goho);
        btn_goho.setOnClickListener(this);
        tv_userid = view.findViewById(R.id.tv_userid);
        edt_friendid = view.findViewById(R.id.edt_friendid);
//        edt_friendid.addTextChangedListener(watcher);
    }

    private TextWatcher watcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            isClickButton = false;
            groupPresenter.validateParameter(tv_userid.getText().toString(), edt_friendid.getText().toString());
        }
    };

    private void setGroupView(View view) {


        LayoutInflater inflater = LayoutInflater.from(context);
        View inflatedLayout = inflater.inflate(R.layout.group_screen, null, false);
        if (content_frame_dialg.getChildCount() > 0)
            content_frame_dialg.removeAllViews();
        content_frame_dialg.addView(inflatedLayout);

//        scrollViewNested = view.findViewById(R.id.scrollViewNested);
//        scrollViewNested.setEnableScrolling(false);
//        scrollViewNested.setNestedScrollingEnabled(false);
//        dialog = view.findViewById(R.id.constraintLay_group);

        rl_groups.setBackgroundColor(getResources().getColor(R.color.black));
        tv_goho.setTextColor(getResources().getColor(R.color.black));
        tv_group.setTextColor(getResources().getColor(R.color.white));
        tv_goho.setBackgroundColor(getResources().getColor(R.color.light_gray));
        img_close.setImageResource(R.drawable.close_a);

        txt_addgroup = view.findViewById(R.id.txt_addgroup);
        tv_viewall = view.findViewById(R.id.tv_viewall);
        tv_viewall.setOnClickListener(this);
        tv_selctall = view.findViewById(R.id.tv_selctall);
        chk_contact_slect_all = view.findViewById(R.id.chk_contact_slect_all);
        chk_contact_slect_all.setOnClickListener(this);

        rv_groupList = view.findViewById(R.id.rv_groupList);
        rv_contactList = view.findViewById(R.id.rv_contactList);

        layoutManager = new LinearLayoutManager(context);
        rv_groupList.setLayoutManager(layoutManager);

        groupAdapter = new GroupAdapter(context, groupArrayList, new OnClickerListener() {
            @Override
            public void onClick(int pos) {

            }
        });


        layoutManager2 = new LinearLayoutManager(context);
        rv_contactList.setLayoutManager(layoutManager2);

        contactAdapter = new ContactAdapter(context, contactArrayList, new OnClickerListener() {
            @Override
            public void onClick(int pos) {

            }
        });

        if (intialTime == 500) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    intialTime = 0;
                    rv_groupList.setAdapter(groupAdapter);
                    rv_contactList.setAdapter(contactAdapter);
                }
            }, intialTime);
        } else {
            rv_groupList.setAdapter(groupAdapter);
            rv_contactList.setAdapter(contactAdapter);
        }
    }

    @Override
    public void onClick(View view) {
        utilClass.perfomroneClick();
        switch (view.getId()) {
            case R.id.tv_viewall:
                if (!isViewAll) {
                    tv_viewall.setText(context.getResources().getString(R.string.closeall));
                    LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) rv_groupList.getLayoutParams();
                    params.height = LinearLayout.LayoutParams.WRAP_CONTENT;
                    rv_groupList.setLayoutParams(params);
                    isViewAll = true;

                    rv_contactList.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
                        @Override
                        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
                            boolean requestCancelDisallowInterceptTouchEvent = rv_contactList.getScrollState() == RecyclerView.SCROLL_STATE_SETTLING;
                            final int action = e.getActionMasked();

                            switch (action) {
                                case MotionEvent.ACTION_DOWN:
                                    if (requestCancelDisallowInterceptTouchEvent) {
//                                        rv_contactList.getParent().requestDisallowInterceptTouchEvent(false);

                                    }
                                    break;
                            }

                            return false;

                        }

                        @Override
                        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
// Handle on touch events here
                            int action = e.getAction();
                            switch (action) {
                                case MotionEvent.ACTION_DOWN:

                                    rv.getParent().requestDisallowInterceptTouchEvent(true);

                                    break;

                                case MotionEvent.ACTION_UP:

                                    rv.getParent().requestDisallowInterceptTouchEvent(false);

                                    break;

                                case MotionEvent.ACTION_MOVE:

                                    rv.getParent().requestDisallowInterceptTouchEvent(true);

                                    break;
                                case MotionEvent.ACTION_SCROLL:

                                    rv.getParent().requestDisallowInterceptTouchEvent(true);
                                    rv_contactList.setNestedScrollingEnabled(true);
                                    break;
                            }
                        }

                        @Override
                        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

                        }
                    });


                } else {
                    tv_viewall.setText(context.getResources().getString(R.string.viewall));
                    LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) rv_groupList.getLayoutParams();
                    params.height = 240;
                    rv_groupList.setLayoutParams(params);
                    isViewAll = false;
                }

//                NestedScrollView.LayoutParams layoutParams=new NestedScrollView.LayoutParams( NestedScrollView.LayoutParams.MATCH_PARENT, NestedScrollView.LayoutParams.WRAP_CONTENT);
//                NestedScrollView scrollView=new NestedScrollView(context);
//                scrollView.setLayoutParams(layoutParams);
//
//                scrollView.addView(dialog);

                break;
            case R.id.chk_contact_slect_all:
                if (!isChkAll) {
                    for (int p = 0; p < contactArrayList.size(); p++) {
                        Friend friend = contactArrayList.get(p);
                        friend.setChk(true);
                        contactArrayList.set(p, friend);
                    }
                    isChkAll = true;
                } else {
                    for (int p = 0; p < contactArrayList.size(); p++) {
                        Friend friend = contactArrayList.get(p);
                        friend.setChk(false);
                        contactArrayList.set(p, friend);
                    }
                    isChkAll = false;
                }
                contactAdapter.notifyDataSetChanged();
                break;
            case R.id.tv_copy:
                UtilClass.hideSoftKeyboard(activity);
//                if(!groupPresenter.isEmpty(tv_userid.getText().toString())) {
                ClipboardManager clipboard = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("your_text_to_be_copied", tv_userid.getText().toString());
                clipboard.setPrimaryClip(clip);
                Snackbar snackbar = Snackbar
                        .make(activity.findViewById(R.id.constaint_main_view), context.getResources().getString(R.string.textcopy), Snackbar.LENGTH_LONG);
                snackbar.setActionTextColor(Color.RED);
                View snackbarView = snackbar.getView();
                snackbarView.setBackgroundColor(context.getResources().getColor(R.color.black));
                TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
                textView.setTextColor(Color.WHITE);
                textView.setTextSize(18);
                snackbar.show();
//                }
                break;
            case R.id.btn_goho:
                isClickButton = true;
                groupPresenter.validateParameter(tv_userid.getText().toString(), edt_friendid.getText().toString());
                break;
        }
    }

    @Override
    public void success() {
        if (isClickButton) {
            FragmentManager fm1 = getFragmentManager();
            UtilClass.hideSoftKeyboardDialog(edt_friendid);
//        ShareOptionDialogFragment shareOptionDialogFragment = new ShareOptionDialogFragment();
//        shareOptionDialogFragment.show(fm1, "dialog");
            isPressedButton = true;
            callBackInterface.onback();
            getDialog().dismiss();
        } else {
            UtilClass.hideSoftKeyboardDialog(edt_friendid);
        }
    }

    @Override
    public void fail(String msg) {

        if (isClickButton) {
            UtilClass.hideSoftKeyboardDialog(edt_friendid);
            UtilClass.showBar(constraintLay_container, msg, context);
        } else {
            UtilClass.showToast(context, msg);
        }
//        getDialog().dismiss();
    }

    CallBackInterface callBackInterface;

    public void setCallBack(CallBackInterface callBackInterface) {
        this.callBackInterface = callBackInterface;
    }
}
