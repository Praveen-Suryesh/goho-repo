package com.inpetrosys.goho.fragment.signup;

import android.content.Context;
import android.text.TextUtils;

import com.inpetrosys.goho.Interfaceuser.Presenterinterface;
import com.inpetrosys.goho.R;
import com.inpetrosys.goho.utility.UtilClass;

public class SignupPresenter3 {
    private Presenterinterface signupinterface3;
    private Context context;

    SignupPresenter3(Context context,Presenterinterface signupinterface3) {
        this.signupinterface3 = signupinterface3;
        this.context = context;
    }
    public void validateParameter(String mobileNumber){
        if (signupinterface3 != null) {
            if (TextUtils.isEmpty(mobileNumber)) {
                signupinterface3.fail(context.getString(R.string.mobnumvalide));
                return;
            } else if (!UtilClass.isValidMobile(mobileNumber)) {
                signupinterface3.fail(context.getString(R.string.validmobnum));
                return;
            } else {
                signupinterface3.success();
                return;
            }
        }
    }
}
