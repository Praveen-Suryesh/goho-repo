package com.inpetrosys.goho.fragment;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.inpetrosys.goho.Interfaceuser.OnClickerListener;
import com.inpetrosys.goho.R;
import com.inpetrosys.goho.activity.HomeActivity;
import com.inpetrosys.goho.adapter.ContactAdapter;
import com.inpetrosys.goho.beans.Friend;

import java.util.ArrayList;

public class ContactListFragment extends DialogFragment {
    private HomeActivity activity;
    private Context context;
    private TextView tv_addGroup;
    private EditText edt_search;
    private RecyclerView rv_contactList;

    private ArrayList<Friend> contactArrayList = new ArrayList<>();
    private LinearLayoutManager linearLayoutManager;
    private ContactAdapter contactAdapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = activity = (HomeActivity) getActivity();
        contactArrayList=getArguments().getParcelableArrayList("contact");
    }
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        setStyle(DialogFragment.STYLE_NO_TITLE, getTheme());
        return super.onCreateDialog(savedInstanceState);

    }
    @Override
    public void onResume() {
        super.onResume();
        int width = getResources().getDisplayMetrics().widthPixels;
        int height = getResources().getDisplayMetrics().heightPixels;
        getDialog().getWindow().setLayout(width, (height-100));
//        getDialog().getWindow().setGravity(Gravity.TOP);
//        new LoadDataAsynktask().execute();
    }

    @Override
    public void onStart() {
        super.onStart();

    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.contact_list_screen, container, false);
        initView(view);
        return view;
    }

    private void initView(View view) {
        tv_addGroup = view.findViewById(R.id.tv_addGroup);
        edt_search = view.findViewById(R.id.edt_search);
        rv_contactList = view.findViewById(R.id.rv_contactList2);


//        for (int i = 0; i < 50; i++) {
//            Group group = new Group();
//            ArrayList<Friend> friendArrayList = new ArrayList<>();
//            friendArrayList.add(new Friend());
//            friendArrayList.add(new Friend());
//            group.setFriendArrayList(friendArrayList);
//            groupArrayList.add(group);
//        }

        linearLayoutManager = new LinearLayoutManager(context);
        rv_contactList.setLayoutManager(linearLayoutManager);
        contactAdapter = new ContactAdapter(context, contactArrayList, new OnClickerListener() {
            @Override
            public void onClick(int pos) {

            }
        });
        rv_contactList.setAdapter(contactAdapter);


    }
    private boolean isPressedButton;
    @Override
    public void onDestroy() {
        super.onDestroy();
        if (!isPressedButton) {
//            Blurry.delete(constaint_main_view);
            ImageView imageView = activity.findViewById(R.id.img_screenshots);
            imageView.setImageBitmap(null);
        }
    }

    ProgressDialog progressBar;
    public class LoadDataAsynktask extends AsyncTask<Void, Void, Boolean> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar = new ProgressDialog(context);
            progressBar.setCancelable(true);//you can cancel it by pressing back button
            progressBar.setMessage("File downloading ...");
            progressBar.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            progressBar.setProgress(0);//initially progress is 0
            progressBar.setMax(100);//sets the maximum value 100
            progressBar.show();//displays the progress bar
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            boolean isTrue = loadData();
            return isTrue;
        }

        @Override
        protected void onPostExecute(Boolean aVoid) {
            super.onPostExecute(aVoid);
            progressBar.hide();
            contactAdapter.notifyDataSetChanged();
        }
    }

    private boolean loadData() {
        for (int i = 0; i < 50; i++) {
            contactArrayList.add(new Friend());
            contactArrayList.add(new Friend());
        }

        return true;
    }
}
