package com.inpetrosys.goho.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.inpetrosys.goho.R;
import com.inpetrosys.goho.activity.StartActivity;

public class VerifyOTPFragment extends Fragment {
    private StartActivity activity;
    private Context context;
    private TextView txt_code;
    private Button btn_verify;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context=activity=(StartActivity) getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.otp_screen,container,false);
        initView(view);
        return view;
    }

    private void initView(View view) {
        txt_code=view.findViewById(R.id.txt_code);
        btn_verify=view.findViewById(R.id.btn_verify);

        btn_verify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.replaceFragment(new ResendOTPFragment());
            }
        });
    }
}
