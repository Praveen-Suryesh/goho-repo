package com.inpetrosys.goho.fragment.signup;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.inpetrosys.goho.R;
import com.inpetrosys.goho.activity.StartActivity;

public class SignupFragment1 extends Fragment {
    private StartActivity activity;
    private Context context;
    private EditText txt_name;
    private ImageButton img_nxt;
    private ConstraintLayout constraintLay_signup;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context=activity=(StartActivity) getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.signup_screen, container, false);
      try {
          initView(view);
      }catch (Exception e){
          e.printStackTrace();
      }
        return view;
    }

    private void initView(View view) {
        txt_name = view.findViewById(R.id.txt_name);
        img_nxt = view.findViewById(R.id.img_nxt);
        constraintLay_signup=view.findViewById(R.id.constraintLay_signup);
        img_nxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!TextUtils.isEmpty(txt_name.getText().toString())) {
                    SignupFragment2 fragment2 = new SignupFragment2();
                    Bundle bundle = new Bundle();
                    bundle.putString("name", txt_name.getText().toString());
                    fragment2.setArguments(bundle);
                    activity.replaceFragment(fragment2);
                }else{
                    Snackbar snackbar = Snackbar
                            .make(constraintLay_signup, context.getResources().getText(R.string.entername), Snackbar.LENGTH_LONG);
                    snackbar.setActionTextColor(Color.RED);
                    View snackbarView = snackbar.getView();
                    snackbarView.setBackgroundColor(context.getResources().getColor(R.color.black));
                    TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
                    textView.setTextColor(Color.WHITE);
                    textView.setTextSize(18);
                    snackbar.show();
                }
            }
        });
    }
}
